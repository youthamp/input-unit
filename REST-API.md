# Authorization
All endpoints are secured. In order to access resources or controll the botConfig manager, successful authentiation is required.
It is sufficient to add one of the following authorization headers:

[1) Basic authentication](https://en.wikipedia.org/wiki/Basic_access_authentication)
```
Authorization Basic <credentials>
```
[2) Bearer token](https://swagger.io/docs/specification/authentication/bearer-authentication/) (using a custom generated jwt access token)
 
```
Authorization Bearer <acess-token>
```

## [Get jwt access token]()
To retrieve a jwt access token add the basic authentication header 

```
Authorization Basic <credentials>
```
to the following request

```
GET /youthamp/input-unit/authentication/token
```
 
Notes 
* The `<credentaials>` has to be a base64 encoding of your username and password in the following format: base64("username:password"). 
Example on linux systems: `echo -n '<username>:<password>' | openssl base64`
* Credentials for the admin user can be found in `<path-to-project-directory>/backend/src/main/resources/credentials.properties`
* For every successful authenticated request via the `Authorization Bearer <acess-token>` header, you always get a new refreshed
access token back in the response header with refreshed expiration time. 


# Bot Manager API

## [Get all botConfig configurations]() 

##### Example Request

```
GET /youthamp/input-unit/botConfig/configuration
```

##### Example Response

```
[
    {
        "id": 1,
        "name": "bot1",
        "active": false,
        "executionCount": 1,
        "failedExecutions": 0,
        "successfulExecutions": 1,
        "lastExecution": "13-03-2021 12:30:00",
        "botPlatform": "twitter",
        "scheduleType": "single",
        "executionInterval": null,
        "properties": {
            "hashtags": "",
            "searchType": "personal timeline"
        }
    },
    {
        "id": 2,
        "name": null,
        "active": true,
        "executionCount": 1,
        "failedExecutions": 0,
        "successfulExecutions": 1,
        "lastExecution": "13-03-2021 12:30:10",
        "botPlatform": "twitter",
        "scheduleType": "interval",
        "executionInterval": "00:30",
        "properties": {
            "hashtags": "test",
            "searchType": "hashtags"
        }
    }
]
```


## [Update botConfig configuration]() 

##### Example Request

```
PUT /youthamp/input-unit/botConfig/configuration?botConfigId=<id>

{
   "name": <botConfig-name>,
   "botPlatform":"twitter",
   "scheduleType":"interval",
   "executionInterval":1800,
   "properties":{
      "searchType":"personal timeline"
   }
}
```

## [Create botConfig configuration]() 

##### Example Request

```
POST /youthamp/input-unit/botConfig/configuration

{
   "name": <botConfig-name>,
   "botPlatform":"twitter",
   "scheduleType":"interval",
   "executionInterval":1800,
   "properties":{
      "searchType":"personal timeline"
   }
}
```
## [Delete botConfig configuration]() 
##### Example Request

```
DELETE /youthamp/input-unit/botConfig/configuration?botConfigId=<id>
```

## [Activate botConfig configuration]() 
Activate the given botConfig configuration so it can be executed.
##### Example Request
```
GET /youthamp/input-unit/botConfig/configuration/activate?botConfigId=<id>
```

## [Deactivate botConfig configuration]() 
Deactivate the given botConfig configuration so it can not longer be executed.
##### Example Request
```
GET /youthamp/input-unit/botConfig/configuration/deactivate?botConfigId=<id>
```

# Twitter API

## [Get statistics of a twitter botConfig]() <a name="botConfig-statistics"></a>
Get execution data of a twitter botConfig for further requests. Note that the tweet ids are hashed
values and can only be used to reference tweets collected by jobs of the input unit. 
##### Example Request
```
GET /youthamp/input-unit/data/twitter/twitter-botConfig-statistics?botConfigId=<id>
```
##### Example Response
```
[
    {
        "collectedOn": "2021-03-13T12:14:30.378+00:00",
        "collectedBy": 5,
        "tweetIds": [
            "42569e7feffda2b2cfd725b07f88c800d06bd54bb89cdfc6be7af3a584b33612",
            "4401fe19ef360ecac435d8dd28f51681b6a841562e80438d7471268b3a443c5b",
            "077ea76ca60bd201c3d0f451ad9708aaa8dd5ff955288ed05c9004f2171c46cb",
            "18cc70af8b0e40c2562ef26a7983b8aae47140e70591bbf985c325c76bc19446",
            "2aafcfde3bda57612a9ea186dedf559330b2511eaffd257e6a24ec36ee668d01",
            "49061f915a2a5471dd93949428450237a3bfd0ac80304f8a50c207e3142c51ff",
            "6a426561f172f7c79d9e9d34d6532cb523290394cef017603e1e59d09549acdc",
            "593bffe7d96b688a9a0659883865a33b61767d1803b9d3d176a7334f5e203693"
        ],
        "collectedTweets": 8
    },
    {
        "collectedOn": "2021-03-13T12:15:30.312+00:00",
        "collectedBy": 5,
        "tweetIds": [
            "de01841775459efa5f9520d1d74e648a4bc363c94383552352a31302c4d56253",
            "da61b28561330ac2cda04c77cfc05bc92ef9107fb743e0532a363a870a691fbc",
            "586e467700958fc83fb2caf135a933928878e7c985f4de743b58b0fe4e0a463f",
            "20d171b228d5f624eb7e044279aef88d0d661d9c80dca0db21487b4953ff92e0",
            "4af6df699b2bdb2982787f162f3faa081a482af5c99873ddfca92dbd7355eda3",
            "da685ac5cf775dd070afef4365ff25daeee6b98277af28e06007bf6dd04fad02",
            "d893f0d1afa320e9d538ba7d701563583bf9a21bb38f0300130b203daf83aabc",
            "16a54db8661b7f45e612a86b691bc81bd2b1b1ca0d7c99167a15d62e17a85213",
            "02dab7244f97b71eb2f6da10f8abf8339f92d29c802108e9f5f13f2090f66ccc",
            "f7979473a17a96c039464f9d9d0b99076253de3793ce1a7f4f735c5c66de1f4b",
            "d72470bd9a585af68d5cec6345fa419cc77321df87895daa029b68af6f38c5b7",
            "760a86c7ae20af2b44c9321175aa32a56c32530ca0e508ba2faa24f8db2ed66e",
            "39d64eee7f4f75128899f11bbd5f4d01a905df629ad0873facd6a8866d9e7a6e",
            "f96adc7c89f86ea95446c01fe081b2aa284da932e424380d44585243c1268cbe",
            "2db94b14485b93c575e5b52b6deb60611db5e0550b1bfc1fb9db523551e47584"
        ],
        "collectedTweets": 15
    }
]
```

## [Get all tweets after a given date]() 
Get all tweets collected after a given date and/or collected by a specific botConfig. At least one of
the following request parameters have to be provided:

* Date string 'after' in the following format: dd.MM.yyy
* Bot config id 'botConfigId' 

##### Example Request
```
GET /youthamp/input-unit/data/twitter/tweets?after=01.01.2000&botConfigId=1
```

## [Get specific subset of tweets collected by a twitter botConfig]() 
Use this endpoint to retrieve tweets which have been collected on a certain date by the given
twitter botConfig. The data needed for this request can be requested using [this endpoint](#botConfig-statistics).
##### Example Request
```
POST /youthamp/input-unit/data/twitter/tweets

Request body: {"collectedBy":"5","collectedOn":"2021-03-13T12:15:30.312+00:00"}
```
##### Example Response
```
[
    {
        "id": "de01841775459efa5f9520d1d74e648a4bc363c94383552352a31302c4d56253",
        "collectedBy": 5,
        "collectedOn": "2021-03-13T12:15:30.312+00:00",
        "user": {
            "hashedUserId": "78d38461934d75edde62e0314c5468f5ef93ab226e45e9fc7d155cd76612428f",
            "followersCount": 740,
            "friendsCount": 922,
            "createdAt": "2021-03-13T12:22:09.000+00:00",
            "lang": null,
            "location": "Am Neckartor"
        },
        "message": "#Rostock: #Autokorso gegen die #Corona-#Diktatur des #Merkel-#Regime\n\nhttps://t.co/ySM7DtrbaR",
        "lang": "de",
        "favoriteCount": 1,
        "retweetCount": 0,
        "retweetFavoriteCount": 0,
        "createdAt": "2021-03-13T12:14:19.000+00:00"
    },
    {
        "id": "586e467700958fc83fb2caf135a933928878e7c985f4de743b58b0fe4e0a463f",
        "collectedBy": 5,
        "collectedOn": "2021-03-13T12:15:30.312+00:00",
        "user": {
            "hashedUserId": "aee916d0fab00f2ed5005d6f02dde3e63a435e7808ed7d774139b9eea7a612d6",
            "followersCount": 263,
            "friendsCount": 1381,
            "createdAt": "2021-03-13T12:15:00.000+00:00",
            "lang": null,
            "location": ""
        },
        "message": "„Unsere Ozeane sind keine Müllkippe! Frau Merkel stoppen Sie diesen Irrsinn!” - Jetzt unterschreiben! https://t.co/lasUZfvUmY via @ChangeGER",
        "lang": "de",
        "favoriteCount": 0,
        "retweetCount": 0,
        "retweetFavoriteCount": 0,
        "createdAt": "2021-03-13T12:15:00.000+00:00"
    },
    ...
]
``