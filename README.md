# Youthamp input-unit

This repository contains a project setup for a youthamp input-unit project.
It is intended to be forked and developed by anyone who is interested in. 

## 1 Prerequisite
Following tools must be installed for local development 
- Node.js (tested with: 12.18.3): https://nodejs.org/en/
- docker-compose (tested with: 1.27.4): https://docs.docker.com/compose/install/
- Docker (tested with: 19.03.13): https://docs.docker.com/get-docker/
- Angular CLI (tested with: 11.2.1):  https://angular.io/
- PostgreSQL (tested with: 13.2): https://www.postgresql.org/download/
- maven (tested with: 3.6.3_1): https://maven.apache.org/install.html
- OpenJDK 13.0.2 

## 2 Development

### PostgreSQL
Independently on how you want to start the application, make sure that PostgreSQL is running and configured correctly according 
to the application properties in ```<path-to-project-directory>/backend/src/main/resources/application-<profile>.properties```

### Credential properties
Credentials and other sensible information should be stored inside the file ```<path-to-project-directory>/backend/src/main/resources/credentials.properties```. This file is 
by default ignored from git and thus has to be created. You can copy the template file ```<path-to-project-directory>/backend/src/main/resources/credentials-example.properties```, rename it accordingly and insert your credentials.  

Your have two choices to start the application: 
- 2.1) with docker/docker-compose or 
- 2.2) using the command line with angular CLI and maven 
- 2.3) Inside your IDE

### 2.1 Start with docker/docker-compose

#### Build docker images 

The input-unit consists of a frontend and backend component which have to be build separately.

##### build frontend
    docker build -t input-unit:frontend <path-to-project-directory>/frontend
    
##### build backend
    docker build -t input-unit:backend <path-to-project-directory>/backend

#### start/stop application
    $ cd <path-to-project-directory>/tools/dev
##### start application
    $ ./start-dev.sh
##### stop application
    $ ./stop-dev.sh

### 2.2 Start using the command line with angular CLI and maven 
#### start backend
 
    $ cd <path-to-project-directory>/backend
    $ mvn clean package spring-boot:repackage
    $ java -jar -Dspring.profiles.active=dev target/backend-1.0-SNAPSHOT.jar
    
#### start frontend
 
    $ cd <path-to-project-directory>/frontend
    $ ng serve

### 2.3 Setup and run inside IDE
You can of course setup and run the project inside your IDE. Since the backend is a Spring-Boot application with the build tool maven 
and the frontend is an angular application, the setup is well known and should be followed by the general guide lines and the IDE
which is used.

##### Example Setup with Intellij IDEA 2020.1
- open Intellij
- file -> new -> project from existing source -> select <path-to-project-directory>
- open project settings -> click "+" to import backend module -> select backend -> select "import module from external model" and 
select "maven"
- open project settings -> click "+" to import frontend module -> select frontend -> select "import module from existing source" 
- open project settings -> set project SDK and project language level to Java 13
- open run configurations -> set main class to 'app.Application' and active profile to 'dev'
- open terminal, cd into /frontend and execute 'ng serve' to start the frontend
- backend can be started through the toolbar

## 3 Open Application in your browser

You are now ready to open the application in your browser: [http://localhost:4200/](http://localhost:4200/). The login credentials
 for the admin user can be found inside the file ```<path-to-project-directory>/backend/src/main/resources/credentials.properties```.

## 4 REST API
The youthamp-input-unit has a REST API which can be used to access data collected by the jobs or to control the botConfig manager.
Detailed documentation can be found in [REST-API.md](REST-API.md). 
