package app.bot.platforms.shared.sentiment.repository;

import app.bot.platforms.shared.sentiment.domain.GoogleSentiment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SentimentRepository extends JpaRepository<GoogleSentiment, String> {
}
