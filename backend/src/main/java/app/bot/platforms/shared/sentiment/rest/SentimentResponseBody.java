package app.bot.platforms.shared.sentiment.rest;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SentimentResponseBody {
    private String id;
    private float magnitude;
    private float score;
}
