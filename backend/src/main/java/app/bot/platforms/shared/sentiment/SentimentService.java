package app.bot.platforms.shared.sentiment;

import app.bot.platforms.instagram.domain.Post;
import app.bot.platforms.instagram.repository.InstagramPostRepository;
import app.bot.platforms.shared.sentiment.domain.DataType;
import app.bot.platforms.shared.sentiment.domain.GoogleSentiment;
import app.bot.platforms.shared.sentiment.rest.SentimentStatisticsResponseBody;
import app.bot.platforms.twitter.domain.Tweet;
import app.bot.platforms.twitter.repositories.TweetRepository;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.language.v1.Document;
import com.google.cloud.language.v1.LanguageServiceClient;
import com.google.cloud.language.v1.LanguageServiceSettings;
import com.google.cloud.language.v1.Sentiment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SentimentService {

    private final static Logger log = LoggerFactory.getLogger(SentimentService.class);

    private LanguageServiceClient client;

    private final TweetRepository tweetRepository;
    private final InstagramPostRepository instagramPostRepository;

    public SentimentService(Environment environment, TweetRepository tweetRepository,
                            InstagramPostRepository instagramPostRepository) {
        this.tweetRepository = tweetRepository;
        this.instagramPostRepository = instagramPostRepository;
        try{
            var pathToCredentialsFile = environment.getProperty("google.cloud.credentials_file", "");
            var credentialsResource = new ClassPathResource(pathToCredentialsFile);
            var credentials = GoogleCredentials.fromStream(credentialsResource.getInputStream());
            var credentialsProvider = FixedCredentialsProvider.create(credentials);
            var settings = LanguageServiceSettings.newBuilder().setCredentialsProvider(credentialsProvider).build();
            client = LanguageServiceClient.create(settings);
        } catch (IOException e){
            log.error("Failed to instantiate google language client: {}", e.getMessage());
        }
    }


    public Optional<Sentiment> analyzeSentiment(String text) {
        try{
            return Optional.of( client.analyzeSentiment(Document.newBuilder()
                                                                .setContent(text)
                                                                .setType(Document.Type.PLAIN_TEXT)
                                                                .build())
                                      .getDocumentSentiment());
        }catch (NullPointerException e){
            log.error("Failed to retrieve google sentiment. Client might not be initialized: {}", e.getMessage());
            return Optional.empty();
        }
    }

    public SentimentStatisticsResponseBody calculateStatistics(String searchTerm, DataType dataType){
        List<GoogleSentiment> sentiments;
        if (dataType.equals(DataType.TWEET)) {
            sentiments = tweetRepository.findAllBySentimentIdIsNotNullAndMessageContains(searchTerm)
                                        .stream()
                                        .map(Tweet::getSentiment)
                                        .collect(Collectors.toList());
            return createStatisticsFrom(sentiments).dataType(DataType.TWEET).build();

        } else {
            sentiments = instagramPostRepository.findAllBySentimentIdIsNotNullAndCaptionTextContains(searchTerm)
                                        .stream()
                                        .map(Post::getSentiment)
                                        .collect(Collectors.toList());
            return createStatisticsFrom(sentiments).dataType(DataType.INSTAGRAM_POST).build();
        }
    }

    private SentimentStatisticsResponseBody.SentimentStatisticsResponseBodyBuilder createStatisticsFrom(
            List<GoogleSentiment> sentiments) {
        var positiveCount = 0;
        var negativeCount = 0;
        var avgScore = 0f;
        var avgMagnitude = 0f;

        for (GoogleSentiment sentiment : sentiments) {
            avgScore += sentiment.getScore();
            avgMagnitude += sentiment.getMagnitude();
            if (sentiment.getScore() >= 0) {
                positiveCount++;
            } else {
                negativeCount++;
            }
        }
        var dataSnippets = positiveCount + negativeCount;
        avgMagnitude = avgMagnitude / dataSnippets;
        avgScore = avgScore / dataSnippets;
        return SentimentStatisticsResponseBody.builder()
                                              .avgScore(avgScore)
                                              .avgMagnitude(avgMagnitude)
                                              .dataSnippets(dataSnippets)
                                              .positiveDataSnippets(positiveCount)
                                              .negativeDataSnippets(negativeCount);
    }
}
