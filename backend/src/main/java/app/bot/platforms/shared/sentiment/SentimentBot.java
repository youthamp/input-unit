package app.bot.platforms.shared.sentiment;

import app.bot.core.services.Bot;
import app.bot.platforms.BotGroup;
import app.bot.platforms.BotKey;
import app.bot.platforms.instagram.domain.Post;
import app.bot.platforms.instagram.repository.InstagramPostRepository;
import app.bot.platforms.shared.sentiment.domain.DataType;
import app.bot.platforms.shared.sentiment.domain.GoogleSentiment;
import app.bot.platforms.shared.sentiment.repository.SentimentRepository;
import app.bot.platforms.twitter.domain.Tweet;
import app.bot.platforms.twitter.repositories.TweetRepository;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class SentimentBot implements Bot {

    private final static Logger log = LoggerFactory.getLogger(SentimentBot.class);

    private final TweetRepository tweetRepository;

    private final InstagramPostRepository instagramPostRepository;

    private final SentimentService sentimentService;

    private final SentimentRepository sentimentRepository;

    public SentimentBot(TweetRepository tweetRepository,
                        InstagramPostRepository instagramPostRepository, SentimentService sentimentService,
                        SentimentRepository sentimentRepository) {
        this.tweetRepository = tweetRepository;
        this.instagramPostRepository = instagramPostRepository;
        this.sentimentService = sentimentService;
        this.sentimentRepository = sentimentRepository;
    }

    public static class PropertyKeys {
        public static final String DATA_TYPES = "dataTypes";
    }

    @Override
    public String getKey() {
        return BotKey.GOOGLE_SENTIMENT.getValue();
    }

    @Override
    public String getGroup() {
        return BotGroup.GOOGLE_CLOUD.getValue();
    }

    @Override
    public void interrupt() throws UnableToInterruptJobException {
        log.info("Sentiment bot got interrupted!");
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("Execute google sentiment bot...");
        List<DataType> dataTypes = getDataTypes(context);
        if(dataTypes.isEmpty()){
            throw new RuntimeException("Unabled to execute google sentiment bot: The list of data types is empty.");
        }

        for(DataType dataType: dataTypes){
            switch (dataType){
                case TWEET:
                    for(Tweet tweet: tweetRepository.findAllBySentimentIdIsNull()){
                        try{
                            getSentiment(tweet.getMessage(), DataType.TWEET).ifPresent(googleSentiment -> {
                                sentimentRepository.save(googleSentiment);
                                tweet.setSentiment(googleSentiment);
                                tweetRepository.save(tweet);
                            });
                        }catch (RuntimeException e){
                            log.error("Failed to get google sentiment for tweet {}: {}", tweet.getMessage(), e.getMessage());
                        }
                    }
                    break;
                case INSTAGRAM_POST:
                    for(Post post: instagramPostRepository.findAllBySentimentIdIsNull()){
                       try{
                           getSentiment(post.getCaptionText(), DataType.INSTAGRAM_POST).ifPresent(googleSentiment -> {
                               sentimentRepository.save(googleSentiment);
                               post.setSentiment(googleSentiment);
                               instagramPostRepository.save(post);
                           });
                       }catch (RuntimeException e){
                           log.error("Failed to get google sentiment for instagram post {}: {}", post.getCaptionText(), e.getMessage());
                       }
                    }
                    break;
                default:
                    log.error("Data type <{}> not supported for google sentiment analysis.", dataType);
            }
        }
    }

    private Optional<GoogleSentiment> getSentiment(String text, DataType dataType) {
        return sentimentService.analyzeSentiment(text)
                               .map(sentiment -> GoogleSentiment.builder()
                                                                .magnitude(sentiment.getMagnitude())
                                                                .score(sentiment.getScore())
                                                                .referencedDataType(dataType)
                                                                .build());
    }

    private List<DataType> getDataTypes(JobExecutionContext context){
        List<DataType> dataTypes = new ArrayList<>();
        String dataTypesStr = (String) context.getJobDetail()
                                              .getJobDataMap()
                                              .get(PropertyKeys.DATA_TYPES);

        for (String dataType : dataTypesStr.split(",")) {
            try{
                dataTypes.add(DataType.valueOf(dataType.trim()));
            }catch (RuntimeException e){
                log.debug("Failed to map data type <{}> to valid enum: {}", dataType, e.getMessage());
            }
        }
        return dataTypes;
    }
}
