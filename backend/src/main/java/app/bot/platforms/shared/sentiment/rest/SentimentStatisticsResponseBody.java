package app.bot.platforms.shared.sentiment.rest;

import app.bot.platforms.shared.sentiment.domain.DataType;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SentimentStatisticsResponseBody {
    private DataType dataType;
    private int dataSnippets = 0;
    private int positiveDataSnippets = 0;
    private int negativeDataSnippets = 0;
    private float avgScore = 0;
    private float avgMagnitude = 0;
}
