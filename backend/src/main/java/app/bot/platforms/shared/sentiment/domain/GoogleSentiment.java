package app.bot.platforms.shared.sentiment.domain;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "google_sentiment")
public class GoogleSentiment {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "magnitude")
    private float magnitude;

    @Column(name = "score")
    private float score;

    @Column(name = "referenced_data_type")
    @Enumerated(EnumType.STRING)
    private DataType referencedDataType;

    @UpdateTimestamp
    @Column
    private Timestamp modified;

    @CreationTimestamp
    @Column
    private Timestamp created;
}
