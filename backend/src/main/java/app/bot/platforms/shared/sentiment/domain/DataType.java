package app.bot.platforms.shared.sentiment.domain;

public enum DataType {
    TWEET,
    INSTAGRAM_POST
}
