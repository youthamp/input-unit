package app.bot.platforms.shared.sentiment.rest;

import app.bot.platforms.shared.sentiment.SentimentService;
import app.bot.platforms.shared.sentiment.domain.DataType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static app.system.rest.Constants.Web.SENTIMENT_DATA_URL;

@RestController
@RequestMapping(value = SENTIMENT_DATA_URL)
public class SentimentController {

    private final SentimentService sentimentService;

    public SentimentController(SentimentService sentimentService) {
        this.sentimentService = sentimentService;
    }

    @RequestMapping(value = "/statistics", method = RequestMethod.GET, produces = "application/json")
    public List<SentimentStatisticsResponseBody> getSentimentStatistics(@RequestParam(name = "searchTerm") String searchTerm) {
        List<SentimentStatisticsResponseBody> sentimentResponseBodies = new ArrayList<>(2);
        sentimentResponseBodies.add(sentimentService.calculateStatistics(searchTerm, DataType.TWEET));
        sentimentResponseBodies.add(sentimentService.calculateStatistics(searchTerm, DataType.INSTAGRAM_POST));
        return sentimentResponseBodies;
    }
}
