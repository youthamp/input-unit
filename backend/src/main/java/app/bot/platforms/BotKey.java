package app.bot.platforms;

public enum BotKey {

    TWITTER("twitter"),
    INSTAGRAM("instagram"),
    GOOGLE_SENTIMENT("google_sentiment");

    public String value;

    BotKey(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static BotKey fromValue(String value) {
        for (BotKey platform : BotKey.values()) {
            if (platform.getValue()
                        .equals(value)) {
                return platform;
            }
        }
        throw new IllegalArgumentException();
    }

    @Override
    public String toString() {
        return value;
    }
}
