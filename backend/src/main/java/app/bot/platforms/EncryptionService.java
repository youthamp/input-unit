package app.bot.platforms;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * @author Alexander Voigt
 *
 * Simple encryption service
 */
public class EncryptionService {
    public static String encrypt(long num){
        return encrypt(String.valueOf(num));
    }

    public static String encrypt(String str){
        return DigestUtils.sha256Hex(str);
    }
}
