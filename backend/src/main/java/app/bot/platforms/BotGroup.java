package app.bot.platforms;

public enum BotGroup {

    SOCIAL_NETWORK("social_network"),
    GOOGLE_CLOUD("google_cloud");

    public String value;

    BotGroup(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static BotGroup fromValue(String value) {
        for (BotGroup platform : BotGroup.values()) {
            if (platform.getValue()
                        .equals(value)) {
                return platform;
            }
        }
        throw new IllegalArgumentException();
    }

    @Override
    public String toString() {
        return value;
    }
}
