package app.bot.platforms.twitter.domain;

public enum SearchType {

    PERSONAL_TIMELINE("personal timeline"),
    USER_TIMELINE("user timeline"),
    HASHTAGS("hashtags");

    String searchType;

    SearchType(String searchType){
        this.searchType = searchType;
    }

    public String getSearchType() {
        return searchType;
    }

    public static SearchType fromValue(String value){
        for (SearchType searchType : SearchType.values()) {
            if (searchType.getSearchType()
                            .equals(value)) {
                return searchType;
            }
        }
        throw new IllegalArgumentException();
    }
}
