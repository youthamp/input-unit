package app.bot.platforms.twitter.domain;

import app.bot.platforms.EncryptionService;
import twitter4j.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class TwitterUser {

    @Id
    @Column(name = "user_id")
    private String userId;

    @Column(name = "screen_name")
    private String screenName;

    @Column(name = "followers_count")
    private long followersCount;

    @Column(name = "friends_count")
    private long friendsCount;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "lang")
    private String lang;

    @Column(name = "location")
    private String location;

    public TwitterUser() {

    }

    TwitterUser(String userId, String screenName, long followersCount, long friendsCount,
                Date createdAt, String lang, String location) {
        this.userId = userId;
        this.followersCount = followersCount;
        this.friendsCount = friendsCount;
        this.createdAt = createdAt;
        this.lang = lang;
        this.location = location;
    }

    public static TwitterUser createFromUser(User user) {
        return TwitterUser.builder()
                          .fromUser(user)
                          .build();
    }

    public static Builder builder() {
        return new Builder();
    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String id) {
        this.userId = EncryptionService.encrypt(id);
    }

    public long getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(long followersCount) {
        this.followersCount = followersCount;
    }

    public long getFriendsCount() {
        return friendsCount;
    }

    public void setFriendsCount(long friendsCount) {
        this.friendsCount = friendsCount;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getScreenName() {
        return this.screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public static class Builder {
        private String id;
        private String screenName;
        private long followersCount;
        private long friendsCount;
        private Date createdAt;
        private String lang;
        private String location;

        public Builder() {
        }

        public Builder setId(String id) {
            this.id = EncryptionService.encrypt(id);
            return this;
        }

        public Builder setId(long id) {
            this.id = EncryptionService.encrypt(id);
            return this;
        }

        public Builder setScreenName(String screenName) {
            this.screenName = screenName;
            return this;
        }

        public Builder setFollowersCount(long followersCount) {
            this.followersCount = followersCount;
            return this;
        }

        public Builder setFriendsCount(long friendsCount) {
            this.friendsCount = friendsCount;
            return this;
        }

        public Builder setCreatedAt(Date createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder setLang(String lang) {
            this.lang = lang;
            return this;
        }

        public Builder setLocation(String location) {
            this.location = location;
            return this;
        }

        public Builder fromUser(User user) {
            this.setId(user.getId())
                .setFollowersCount(user.getFollowersCount())
                .setFriendsCount(user.getFriendsCount())
                .setCreatedAt(user.getCreatedAt())
                .setLang(user.getLang())
                .setLocation(user.getLocation());
            return this;
        }

        public TwitterUser build() {
            return new TwitterUser(id, screenName, followersCount, friendsCount, createdAt, lang,
                                   location);
        }
    }
}
