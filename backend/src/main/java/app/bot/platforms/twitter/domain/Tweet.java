package app.bot.platforms.twitter.domain;

import app.bot.platforms.EncryptionService;
import app.bot.platforms.shared.sentiment.rest.SentimentResponseBody;
import app.bot.platforms.twitter.rest.dto.TweetResponseBody;
import app.bot.platforms.shared.sentiment.domain.GoogleSentiment;
import lombok.Getter;
import lombok.Setter;
import twitter4j.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
public class Tweet implements Serializable {

    @Id
    private String id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private TwitterUser user;

    @Column(name = "message", length = 512)
    private String message;

    @Column(name = "bot_execution_id")
    private String botExecutionId;

    @Column(name = "lang")
    private String lang;

    @Column(name = "favorite_count")
    private long favoriteCount;

    @Column(name = "retweet_count")
    private long retweetCount;

    @Column(name = "retweet_favorite_count")
    private long retweetFavoriteCount;

    @Column(name = "created_at")
    private Date createdAt;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sentiment_id")
    private GoogleSentiment sentiment;

    public Tweet() {

    }

    public Tweet(String id, TwitterUser user, String message, String botExecutionId, String lang,
                 long favoriteCount, long retweetCount, long retweetFavoriteCount, Date createdAt,
                 GoogleSentiment sentiment) {
        this.id = id;
        this.user = user;
        this.message = message;
        this.botExecutionId = botExecutionId;
        this.lang = lang;
        this.favoriteCount = favoriteCount;
        this.retweetCount = retweetCount;
        this.retweetFavoriteCount = retweetFavoriteCount;
        this.createdAt = createdAt;
        this.sentiment = sentiment;
    }

    public TweetResponseBody toTweetResponseBody() {

        var sentimentResponseBody = (sentiment != null) ? SentimentResponseBody.builder()
                                                                               .id(sentiment.getId())
                                                                               .score(sentiment.getScore())
                                                                               .magnitude(sentiment.getMagnitude())
                                                                               .build() : null;
        return TweetResponseBody.builder()
                                .id(id)
                                .userId(user.getUserId())
                                .createdAt(createdAt)
                                .message(message)
                                .lang(lang)
                                .favoriteCount(favoriteCount)
                                .retweetCount(retweetCount)
                                .retweetFavoriteCount(retweetFavoriteCount)
                                .sentiment(sentimentResponseBody)
                                .build();
    }


    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String id;
        private TwitterUser user;
        private String message;
        private String botExecutionId;
        private String lang;
        private long favoriteCount;
        private long retweetCount;
        private long retweetFavoriteCount;
        private Date createdAt;
        private GoogleSentiment sentiment;

        Builder() {
        }

        public Builder setId(long id) {
            this.id = EncryptionService.encrypt(id);
            return this;
        }

        public Builder setUser(TwitterUser user) {
            this.user = user;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setBotExecutionId(String botExecutionId) {
            this.botExecutionId = botExecutionId;
            return this;
        }

        public Builder setLang(String lang) {
            this.lang = lang;
            return this;
        }

        public Builder setFavoriteCount(long favoriteCount) {
            this.favoriteCount = favoriteCount;
            return this;
        }

        public Builder setRetweetCount(long retweetCount) {
            this.retweetCount = retweetCount;
            return this;
        }

        public Builder setRetweetFavoriteCount(long retweetFavoriteCount) {
            this.retweetFavoriteCount = retweetFavoriteCount;
            return this;
        }

        public Builder setCreatedAt(Date createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder setSentiment(GoogleSentiment sentiment) {
            this.sentiment = sentiment;
            return this;
        }

        public Builder fromStatus(Status status) {
            return this.setId(status.getId())
                       .setCreatedAt(status.getCreatedAt())
                       .setFavoriteCount(status.getFavoriteCount())
                       .setLang(status.getLang())
                       .extractMessage(status)
                       .setRetweetCount(status.getRetweetCount())
                       .setRetweetFavoriteCount(getRetweetFavoriteCount(status))
                       .setUser(TwitterUser.createFromUser(status.getUser()));
        }

        private long getRetweetFavoriteCount(Status status) {
            try {
                return status.getRetweetedStatus()
                             .getFavoriteCount();
            } catch (Exception e) {
                return 0;
            }
        }

        public Tweet build() {
            return new Tweet(id, user, message, botExecutionId, lang, favoriteCount, retweetCount,
                             retweetFavoriteCount, createdAt, sentiment);
        }

        private Builder extractMessage(Status status) {
            if (status.getRetweetedStatus() != null) {
                this.message = status.getRetweetedStatus()
                                     .getText();
            } else {
                this.message = status.getText();
            }

            return this;
        }
    }

}
