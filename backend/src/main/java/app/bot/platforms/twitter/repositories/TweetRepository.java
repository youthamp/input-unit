package app.bot.platforms.twitter.repositories;

import app.bot.platforms.twitter.domain.Tweet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface TweetRepository extends PagingAndSortingRepository<Tweet, String> {

    void save(List<Tweet> tweets);

    @Query(value = "DELETE FROM Tweet tweet WHERE tweet.bot_execution_id = :botExecutionId", nativeQuery = true)
    void deleteByBotExecutionId(String botExecutionId);

    @Query(value = "SELECT * FROM tweet WHERE tweet.created_at > :after", nativeQuery = true)
    List<Tweet> findAllCreatedAfter(Date after);

    @Query(value = "SELECT id FROM tweet WHERE tweet.bot_execution_id = :botExecutionId", nativeQuery = true)
    Set<String> findAllByBotExecutionId(String botExecutionId);

    @Query(value = "SELECT * FROM tweet WHERE tweet.bot_execution_id = :botExecutionId", nativeQuery = true)
    Page<Tweet> findAllByBotExecutionId(String botExecutionId, Pageable pageable);

    List<Tweet> findAllBySentimentIdIsNull();

    List<Tweet> findAllBySentimentIdIsNotNullAndMessageContains(String searchTerm);
}
