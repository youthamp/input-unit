package app.bot.platforms.twitter;

import app.bot.core.services.Bot;
import app.bot.core.domain.BotExecution;
import app.bot.core.services.JobExecutionKeys;
import app.bot.core.services.BotExecutionService;
import app.bot.platforms.BotGroup;
import app.bot.platforms.BotKey;
import app.bot.platforms.twitter.domain.SearchType;
import app.bot.platforms.twitter.domain.Tweet;
import app.bot.platforms.twitter.services.TwitterService;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.util.*;

@Component
public class TwitterBot implements Bot {

    private final static Logger log = LoggerFactory.getLogger(TwitterBot.class);

    private Twitter twitterClient;

    private final TwitterService twitterService;

    private final BotExecutionService botExecutionService;

    private final Environment environment;

    private final int DEFAULT_MAX_COUNT = 600;
    private final int DEFAULT_PAGE_SIZE = 200;

    public TwitterBot(Environment environment, TwitterService twitterService,
                      BotExecutionService botExecutionService) {
        this.twitterService = twitterService;
        this.botExecutionService = botExecutionService;
        this.environment = environment;
    }

    @Override
    public String getKey() {
        return BotKey.TWITTER.getValue();
    }

    @Override
    public String getGroup() {
        return BotGroup.SOCIAL_NETWORK.getValue();
    }

    public static class PropertyKeys {
        public static final String HASHTAGS = "hashtags";
        public static final String SCREEN_NAME = "screenName";
        public static final String SEARCH_TYPE = "searchType";
        public static final String MAX_COUNT = "maxCount";
    }

    @Override
    public void execute(JobExecutionContext context) {
        log.info("Execute twitter bot...");
        JobDataMap jobDataMap = context.getJobDetail()
                                       .getJobDataMap();
        initTwitterClient(environment);
        String searchType = (String) jobDataMap.get(PropertyKeys.SEARCH_TYPE);
        if (searchType.equals(SearchType.HASHTAGS.getSearchType())) {
            collectTweetsWithHashtags(context);

        } else if (searchType.equals(SearchType.USER_TIMELINE.getSearchType())) {
            collectTweetsFromUserTimeline(context);

        } else if (searchType.equals(SearchType.PERSONAL_TIMELINE.getSearchType())) {
            collectTweetsFromPersonalTimeline(context);
        } else {
            throw new RuntimeException("Invalid search type '" + searchType + "'.");
        }
    }

    @Override
    public void interrupt() {
        log.info("Twitter bot got interrupted!");
    }

    private void initTwitterClient(Environment environment) {
        String consumerKey = environment.getProperty("twitter.oauth.consumerKey", "");
        String consumerSecret = environment.getProperty("twitter.oauth.consumerSecret", "");
        String accessToken = environment.getProperty("twitter.oauth.accessToken", "");
        String accessTokenSecret = environment.getProperty("twitter.oauth.accessTokenSecret", "");
        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();

        configurationBuilder.setDebugEnabled(true)
                            .setOAuthConsumerKey(consumerKey)
                            .setOAuthConsumerSecret(consumerSecret)
                            .setOAuthAccessToken(accessToken)
                            .setOAuthAccessTokenSecret(accessTokenSecret);

        TwitterFactory twitterFactory = new TwitterFactory(configurationBuilder.build());
        this.twitterClient = twitterFactory.getInstance();
    }

    private void collectTweetsFromPersonalTimeline(JobExecutionContext context) {
        try {
            Paging paging = new Paging();
            paging.setCount(200);
            ResponseList<Status> responseList;
            int pageNumber = 1;
            do {
                paging.setPage(pageNumber);
                responseList = twitterClient.getMentionsTimeline(paging);
                persistTweets(responseList, context);
                pageNumber++;

            } while (responseList.size() > 0);
        } catch (Exception e) {
            String errorMsg = "Error collecting tweets from personal timeline: " + e.getMessage();
            log.error(errorMsg);
            throw new RuntimeException(errorMsg);
        }

    }

    private void collectTweetsWithHashtags(JobExecutionContext context) {
        Query query = new Query();
        query.setQuery(createHashtagsQueryString(context.getJobDetail()
                                                        .getJobDataMap()
                                                        .getString(PropertyKeys.HASHTAGS)));
        QueryResult result;

        try {
            while (query != null) {
                query.setCount(200);
                result = twitterClient.search(query);
                persistTweets(result.getTweets(), context);
                query = result.nextQuery();
            }
        } catch (Exception e) {
            String errorMsg =
                    "Error collecting tweets with query '" + query + "': " + e.getMessage();
            log.error(errorMsg);
            throw new RuntimeException(errorMsg);
        }
    }

    private void collectTweetsFromUserTimeline(JobExecutionContext context) {
        String screenName = context.getJobDetail()
                                   .getJobDataMap()
                                   .getString(PropertyKeys.SCREEN_NAME);
        int maxCount = getMaxCount(context);
        int pageSize = Math.min(maxCount, DEFAULT_PAGE_SIZE);

        Paging paging = new Paging();
        paging.setCount(pageSize);
        Set<Status> statusesPage;
        try {
            int pageNumber = 1;
            int collected = 0;
            do {
                paging.setPage(pageNumber);
                statusesPage = new HashSet<>(twitterClient.getUserTimeline(screenName, paging));
                persistTweets(statusesPage.iterator(), context);
                pageNumber++;
                collected += statusesPage.size();

            } while (statusesPage.size() > 0 && maxCount > collected);

        } catch (Exception e) {
            String errorMsg =
                    "Error collecting tweets from user timeline with screen name '" + screenName +
                            "': " + e.getMessage();
            throw new RuntimeException(errorMsg);
        }
    }

    private int getMaxCount(JobExecutionContext context) {
        try {
            int maxCount = context.getJobDetail()
                                  .getJobDataMap()
                                  .getInt(PropertyKeys.MAX_COUNT);
            return maxCount > 0 ? maxCount : DEFAULT_MAX_COUNT;
        } catch (ClassCastException e) {
            log.warn("Failed to get property value '{}', the default value of {} will be taken.",
                     PropertyKeys.MAX_COUNT, DEFAULT_MAX_COUNT);
            return DEFAULT_MAX_COUNT;
        }
    }

    private void persistTweets(List<Status> statuses, JobExecutionContext context) {
        String botExecutionId = (String) context.get(JobExecutionKeys.BOT_EXECUTION_ID);
        BotExecution botExecution = botExecutionService.findById(botExecutionId)
                                                       .orElseThrow(() -> new RuntimeException(
                                                               "Could not find bot config with id" +
                                                                       " " + botExecutionId));
        statuses.stream()
                .filter(status -> !twitterService.exists(status.getId(), true))
                .map(status -> transformToTweet(status, botExecution))
                .filter(Objects::nonNull)
                .forEach(twitterService::save);

    }

    private void persistTweets(Iterator<Status> statusIterator, JobExecutionContext context) {
        List<Status> statuses = new ArrayList<>();
        statusIterator.forEachRemaining(statuses::add);
        persistTweets(statuses, context);
    }

    private Tweet transformToTweet(Status status, BotExecution botExecution) {
        try {
            return Tweet.builder()
                        .fromStatus(status)
                        .setBotExecutionId(botExecution.getId())
                        .build();
        } catch (RuntimeException e) {
            log.error("Failed to persist tweet: {}", e.getMessage());
            return null;
        }
    }

    private String createHashtagsQueryString(String hashtagsAsString) {
        if (hashtagsAsString.isEmpty()) {
            throw new RuntimeException(
                    "Search type ist set to '" + SearchType.HASHTAGS.getSearchType() + "' but no" +
                            "hashtags are provided.");
        }
        StringBuilder stringBuilder = new StringBuilder();

        for (String hashtag : hashtagsAsString.split(",")) {
            stringBuilder.append(hashtag.trim());
        }
        return stringBuilder.toString();
    }
}