package app.bot.platforms.twitter.rest.dto;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

public class TwitterBotStatistics {

    private Timestamp collectedOn;

    private String botExecutionId;

    private Set<String> tweetIds;

    private long collectedTweets;

    public TwitterBotStatistics(Timestamp collectedOn, String botExecutionId, Set<String> tweetIds,
                                long collectedTweets) {
        this.collectedOn = collectedOn;
        this.botExecutionId = botExecutionId;
        this.tweetIds = tweetIds;
        this.collectedTweets = collectedTweets;
    }

    public static CollectedTweetsBuilder builder() {
        return new CollectedTweetsBuilder();
    }

    public Date getCollectedOn() {
        return this.collectedOn;
    }

    public String getBotExecutionId() {
        return this.botExecutionId;
    }

    public Set<String> getTweetIds() {
        return this.tweetIds;
    }

    public long getCollectedTweets() {
        return this.collectedTweets;
    }

    public void setCollectedOn(Timestamp collectedOn) {
        this.collectedOn = collectedOn;
    }

    public void setBotExecutionId(String botExecutionId) {
        this.botExecutionId = botExecutionId;
    }

    public void setTweetIds(Set<String> tweetIds) {
        this.tweetIds = tweetIds;
    }

    public void setCollectedTweets(long collectedTweets) {
        this.collectedTweets = collectedTweets;
    }

    public static class CollectedTweetsBuilder {
        private Timestamp collectedOn;
        private String botExecutionId;
        private Set<String> tweetIds;
        private long collectedTweets;

        CollectedTweetsBuilder() {
        }

        public TwitterBotStatistics.CollectedTweetsBuilder collectedOn(Timestamp collectedOn) {
            this.collectedOn = collectedOn;
            return this;
        }

        public TwitterBotStatistics.CollectedTweetsBuilder botExecutionId(String botExecutionId) {
            this.botExecutionId = botExecutionId;
            return this;
        }

        public TwitterBotStatistics.CollectedTweetsBuilder tweetIds(Set<String> tweetIds) {
            this.tweetIds = tweetIds;
            return this;
        }

        public TwitterBotStatistics.CollectedTweetsBuilder collectedTweets(long collectedTweets) {
            this.collectedTweets = collectedTweets;
            return this;
        }

        public TwitterBotStatistics build() {
            return new TwitterBotStatistics(collectedOn, botExecutionId, tweetIds, collectedTweets);
        }

        public String toString() {
            return "CollectedTweets.CollectedTweetsBuilder(collectedOn=" + this.collectedOn +
                    ", botExecutionId=" + this.botExecutionId + ", tweetIds=" + this.tweetIds +
                    ", collectedTweets=" + this.collectedTweets + ")";
        }
    }
}
