package app.bot.platforms.twitter.rest.dto;

import app.bot.platforms.shared.sentiment.rest.SentimentResponseBody;
import app.bot.platforms.shared.sentiment.rest.SentimentStatisticsResponseBody;
import app.bot.platforms.twitter.domain.Tweet;
import lombok.*;

import java.util.Date;

/**
 * @author Alexander Voigt
 * <p>
 * response dto object for {@link Tweet}
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TweetResponseBody {
    private String id;
    private String userId;
    private String message;
    private String lang;
    private long favoriteCount;
    private long retweetCount;
    private long retweetFavoriteCount;
    private Date createdAt;
    private SentimentResponseBody sentiment;
}
