package app.bot.platforms.twitter.rest.dto;

import app.bot.platforms.twitter.domain.Tweet;

public class TweetCsvEntry {
    private String id;

    private String message;

    private String lang;

    private long favoriteCount;

    private long retweetCount;

    private long retweetFavoriteCount;

    private String userHashedId;

    private long userFollowersCount;

    private long userFriendsCount;

    private String userLang;

    private String userLocation;

    TweetCsvEntry(String id, String message, String lang, long favoriteCount, long retweetCount,
                  long retweetFavoriteCount, String userHashedId, long userFollowersCount,
                  long userFriendsCount, String userLang, String userLocation) {
        this.id = id;
        this.message = message;
        this.lang = lang;
        this.favoriteCount = favoriteCount;
        this.retweetCount = retweetCount;
        this.retweetFavoriteCount = retweetFavoriteCount;
        this.userHashedId = userHashedId;
        this.userFollowersCount = userFollowersCount;
        this.userFriendsCount = userFriendsCount;
        this.userLang = userLang;
        this.userLocation = userLocation;
    }

    public static TweetCsvEntry createFrom(Tweet tweet) {
        return TweetCsvEntry.builder()
                            .id(tweet.getId())
                            .favoriteCount(tweet.getFavoriteCount())
                            .lang(tweet.getLang())
                            .message(tweet.getMessage())
                            .retweetCount(tweet.getRetweetCount())
                            .retweetFavoriteCount(tweet.getRetweetFavoriteCount())
                            .userFollowersCount(tweet.getUser()
                                                     .getFollowersCount())
                            .userFriendsCount(tweet.getUser()
                                                   .getFriendsCount())
                            .userHashedId(tweet.getUser()
                                               .getUserId())
                            .userLang(tweet.getUser()
                                           .getLang())
                            .userLocation(tweet.getUser()
                                               .getLocation())
                            .build();

    }

    public static TweetCsvEntryBuilder builder() {
        return new TweetCsvEntryBuilder();
    }

    public String getId() {
        return this.id;
    }

    public String getMessage() {
        return this.message;
    }

    public String getLang() {
        return this.lang;
    }

    public long getFavoriteCount() {
        return this.favoriteCount;
    }

    public long getRetweetCount() {
        return this.retweetCount;
    }

    public long getRetweetFavoriteCount() {
        return this.retweetFavoriteCount;
    }

    public String getUserHashedId() {
        return this.userHashedId;
    }

    public long getUserFollowersCount() {
        return this.userFollowersCount;
    }

    public long getUserFriendsCount() {
        return this.userFriendsCount;
    }

    public String getUserLang() {
        return this.userLang;
    }

    public String getUserLocation() {
        return this.userLocation;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setFavoriteCount(long favoriteCount) {
        this.favoriteCount = favoriteCount;
    }

    public void setRetweetCount(long retweetCount) {
        this.retweetCount = retweetCount;
    }

    public void setRetweetFavoriteCount(long retweetFavoriteCount) {
        this.retweetFavoriteCount = retweetFavoriteCount;
    }

    public void setUserHashedId(String userHashedId) {
        this.userHashedId = userHashedId;
    }

    public void setUserFollowersCount(long userFollowersCount) {
        this.userFollowersCount = userFollowersCount;
    }

    public void setUserFriendsCount(long userFriendsCount) {
        this.userFriendsCount = userFriendsCount;
    }

    public void setUserLang(String userLang) {
        this.userLang = userLang;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    public static class TweetCsvEntryBuilder {
        private String id;
        private String message;
        private String lang;
        private long favoriteCount;
        private long retweetCount;
        private long retweetFavoriteCount;
        private String userHashedId;
        private long userFollowersCount;
        private long userFriendsCount;
        private String userLang;
        private String userLocation;

        TweetCsvEntryBuilder() {
        }

        public TweetCsvEntry.TweetCsvEntryBuilder id(String id) {
            this.id = id;
            return this;
        }

        public TweetCsvEntry.TweetCsvEntryBuilder message(String message) {
            this.message = message;
            return this;
        }

        public TweetCsvEntry.TweetCsvEntryBuilder lang(String lang) {
            this.lang = lang;
            return this;
        }

        public TweetCsvEntry.TweetCsvEntryBuilder favoriteCount(long favoriteCount) {
            this.favoriteCount = favoriteCount;
            return this;
        }

        public TweetCsvEntry.TweetCsvEntryBuilder retweetCount(long retweetCount) {
            this.retweetCount = retweetCount;
            return this;
        }

        public TweetCsvEntry.TweetCsvEntryBuilder retweetFavoriteCount(long retweetFavoriteCount) {
            this.retweetFavoriteCount = retweetFavoriteCount;
            return this;
        }

        public TweetCsvEntry.TweetCsvEntryBuilder userHashedId(String userHashedId) {
            this.userHashedId = userHashedId;
            return this;
        }

        public TweetCsvEntry.TweetCsvEntryBuilder userFollowersCount(long userFollowersCount) {
            this.userFollowersCount = userFollowersCount;
            return this;
        }

        public TweetCsvEntry.TweetCsvEntryBuilder userFriendsCount(long userFriendsCount) {
            this.userFriendsCount = userFriendsCount;
            return this;
        }

        public TweetCsvEntry.TweetCsvEntryBuilder userLang(String userLang) {
            this.userLang = userLang;
            return this;
        }

        public TweetCsvEntry.TweetCsvEntryBuilder userLocation(String userLocation) {
            this.userLocation = userLocation;
            return this;
        }

        public TweetCsvEntry build() {
            return new TweetCsvEntry(id, message, lang, favoriteCount, retweetCount,
                                     retweetFavoriteCount, userHashedId, userFollowersCount,
                                     userFriendsCount, userLang, userLocation);
        }

        public String toString() {
            return "TweetCsvEntry.TweetCsvEntryBuilder(id=" + this.id + ", message=" +
                    this.message + ", lang=" + this.lang + ", favoriteCount=" + this.favoriteCount +
                    ", retweetCount=" + this.retweetCount + ", retweetFavoriteCount=" +
                    this.retweetFavoriteCount + ", userHashedId=" + this.userHashedId +
                    ", userFollowersCount=" + this.userFollowersCount + ", userFriendsCount=" +
                    this.userFriendsCount + ", userLang=" + this.userLang + ", userLocation=" +
                    this.userLocation + ")";
        }
    }
}
