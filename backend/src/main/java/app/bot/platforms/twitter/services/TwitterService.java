package app.bot.platforms.twitter.services;

import app.bot.core.services.BotExecutionService;
import app.bot.core.domain.BotExecution;
import app.bot.core.services.BotConfigService;
import app.bot.platforms.twitter.domain.Tweet;
import app.bot.platforms.twitter.repositories.TweetRepository;
import app.bot.platforms.EncryptionService;
import app.bot.platforms.twitter.rest.dto.TwitterBotStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;


@Service
public class TwitterService {

    private final static Logger log = LoggerFactory.getLogger(TwitterService.class);

    private final TweetRepository tweetRepository;

    private final BotConfigService botConfigService;

    private final BotExecutionService botExecutionService;

    @Autowired
    public TwitterService(TweetRepository tweetRepository, BotConfigService botConfigService,
                          BotExecutionService botExecutionService) {
        this.tweetRepository = tweetRepository;
        this.botConfigService = botConfigService;
        this.botExecutionService = botExecutionService;
    }

    public void save(Tweet tweet) {
        try {
            tweetRepository.save(tweet);
        } catch (Exception e) {
            log.warn("Failed to save tweet: " + e.getMessage());
        }
    }

    public void save(List<Tweet> tweets) {
        try {
            tweetRepository.save(tweets);
        } catch (Exception e) {
            log.warn("Failed to save tweets: " + e.getMessage());
        }
    }

    public boolean exists(long tweetId, boolean encrypt) {
        return exists(String.valueOf(tweetId), encrypt);
    }

    public boolean exists(String tweetId, boolean encrypt) {
        try {
            tweetId = encrypt ? EncryptionService.encrypt(tweetId) : tweetId;
            return tweetRepository.existsById(tweetId);
        } catch (RuntimeException e) {
            return false;
        }
    }

    public void deleteByBotExecution(BotExecution botExecution) {
        try {
            tweetRepository.deleteByBotExecutionId(botExecution.getId());
        } catch (RuntimeException e) {
            log.error("Failed to delete tweets by bot execution id " + botExecution.getId() + ": " +
                              e.getMessage());
        }
    }

    public List<Tweet> getTweets(Date after) {
        return tweetRepository.findAllCreatedAfter(after);

    }

    public Page<Tweet> getTweets(String botExecutionId, Pageable pageable) {
        Page<Tweet> tweetsPage = tweetRepository.findAllByBotExecutionId(botExecutionId, pageable);
        return tweetsPage;

    }

    public List<TwitterBotStatistics> getTwitterBotStatistics(String botConfigId) {
        try {
            return botConfigService.find(botConfigId)
                                   .map(botConfig -> {
                                       Set<String> tweetIds;
                                       List<TwitterBotStatistics> twitterBotStatistics =
                                               new ArrayList<>();

                                       for (BotExecution botExecution :
                                               botExecutionService.findByBotConfigId(botConfigId)) {
                                           Timestamp collectedOn = botExecution.getExecutionStart();
                                           tweetIds = tweetRepository.findAllByBotExecutionId(
                                                   botExecution.getId());
                                           twitterBotStatistics.add(
                                                   new TwitterBotStatistics(collectedOn,
                                                                            botExecution.getId(), tweetIds,
                                                                            tweetIds.size()));
                                       }
                                       return twitterBotStatistics;
                                   })
                                   .orElse(Collections.emptyList());
        } catch (RuntimeException e) {
            log.error("Failed to generate twitter bot statistics: {}", e.getMessage());
            return Collections.emptyList();
        }
    }

    //
    //    public List<Tweet> getTweets(Long collectedBy){
    //        return tweetRepository.findAllByCollectedBy(collectedBy);
    //    }
    //
    //    public List<Tweet> getTweets(Long botConfigId, Timestamp collectedOn){
    //        if(botConfigId == null && collectedOn == null){
    //            return Collections.emptyList();
    //        }
    //
    //        if(collectedOn == null){
    //            return tweetRepository.findAllByCollectedBy(botConfigId);
    //        } else {
    //            return tweetRepository.getTweetsByBotConfigAndDate(botConfigId, collectedOn);
    //        }
    //    }
    //
    //    public Page<Tweet> getTweets(Long collectedBy, Timestamp collectedOn, Pageable pageable){
    //        return tweetRepository.findAllByCollectedByAndCollectedOn(collectedBy, collectedOn,
    //        pageable);
    //    }
    //
    //    public List<TwitterBotStatistics> getTwitterBotStatistics(Long botConfigId){
    //        Set<String> tweetIds;
    //        List<TwitterBotStatistics> twitterBotStatistics = new ArrayList<>();
    //
    //        for(Timestamp collectedOn: tweetRepository.getCollectionTimestamps(botConfigId)){
    //            tweetIds = tweetRepository.getTweetIdsByBotConfigAndDate(botConfigId,
    //            collectedOn);
    //            twitterBotStatistics.add(new TwitterBotStatistics(collectedOn, botConfigId,
    //            tweetIds, tweetIds.size()));
    //        }
    //        return twitterBotStatistics;
    //    }
    //
    //    public boolean deleteAll(List<Tweet> tweets) {
    //        try {
    //            tweetRepository.deleteAll(tweets);
    //            return true;
    //        } catch (IllegalArgumentException e) {
    //            log.error("Failed to delete all tweets: " + e.getMessage());
    //            return false;
    //        }
    //    }
    //
    //    public void writeCsvToResponse(List<Tweet> tweets, HttpServletResponse response) {
    //        List<TweetCsvEntry> tweetCsvEntries = tweets.stream()
    //                                                    .map(TweetCsvEntry::createFrom)
    //                                                    .collect(Collectors.toList());
    //        String errorMessage = "";
    //        final ByteArrayOutputStream out = new ByteArrayOutputStream();
    //        final ObjectMapper mapper = new ObjectMapper();
    //
    //        try {
    //            mapper.writeValue(out, tweetCsvEntries);
    //        } catch (IOException e) {
    //            errorMessage = "Failed write tweets to byte array output stream: " + e
    //            .getMessage();
    //            log.error(errorMessage);
    //        }
    //
    //        JsonNode jsonTree;
    //        try {
    //            jsonTree = new ObjectMapper().readTree(out.toByteArray());
    //        } catch (IOException e) {
    //            errorMessage = "Failed to convert list of tweets to json: " + e.getMessage();
    //            log.error(errorMessage);
    //            throw new MappingException(errorMessage);
    //        }
    //
    //        CsvSchema csvSchema;
    //
    //        try {
    //            CsvSchema.Builder csvSchemaBuilder = CsvSchema.builder();
    //            JsonNode firstObject = jsonTree.elements()
    //                                           .next();
    //            firstObject.fieldNames()
    //
    //                       .forEachRemaining(csvSchemaBuilder::addColumn);
    //            csvSchema = csvSchemaBuilder.build()
    //                                        .withHeader();
    //        } catch (Exception e) {
    //            errorMessage = "Failed to create csv schema: " + e.getMessage();
    //            log.error(errorMessage);
    //            throw new MappingException(errorMessage);
    //        }
    //
    //        CsvMapper csvMapper = new CsvMapper();
    //        try {
    //            response.setContentType("text/csv;charset=utf-8");
    //            csvMapper.writerFor(JsonNode.class)
    //                     .with(csvSchema)
    //                     .writeValue(response.getOutputStream(), jsonTree);
    //        } catch (IOException e) {
    //            errorMessage = "Failed to write csv to response: " + e.getMessage();
    //            log.error(errorMessage);
    //            throw new MappingException(errorMessage);
    //        }
    //    }
}
