package app.bot.platforms.twitter.rest.dto;

import java.sql.Timestamp;

public class TweetQuery {
    private Long collectedBy;
    private Timestamp collectedOn;

    public Long getCollectedBy() {
        return collectedBy;
    }

    public void setCollectedBy(Long collectedBy) {
        this.collectedBy = collectedBy;
    }

    public Timestamp getCollectedOn() {
        return collectedOn;
    }

    public void setCollectedOn(Timestamp collectedOn) {
        this.collectedOn = collectedOn;
    }

}
