package app.bot.platforms.twitter.rest.controllers;

import app.bot.platforms.twitter.domain.Tweet;
import app.bot.platforms.twitter.services.TwitterService;
import app.bot.platforms.twitter.rest.dto.TweetResponseBody;
import app.bot.platforms.twitter.rest.dto.TwitterBotStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static app.system.rest.Constants.Web.TWITTER_DATA_URL;

@RestController
@RequestMapping(TWITTER_DATA_URL)
public class TwitterController {

    private final static Logger logger = LoggerFactory.getLogger(TwitterController.class);

    private final TwitterService twitterService;

    @Autowired
    public TwitterController(TwitterService twitterService) {
        this.twitterService = twitterService;
    }

    @RequestMapping(value = "/tweets", method = RequestMethod.GET, produces = "application/json")
    public List<TweetResponseBody> getTweets(
            @RequestParam(name = "after") @DateTimeFormat(pattern = "dd.MM.yyyy") Date after) {
        logRequest("/tweets");
        return twitterService.getTweets(after)
                             .stream()
                             .map(Tweet::toTweetResponseBody)
                             .collect(Collectors.toList());
    }

    //    @PostMapping(path = "/tweets", consumes = "application/json", produces =
    //    "application/json")
    //    public List<Tweet> getTweets(@RequestBody TweetQuery tweetQuery) {
    //        logRequest("/tweets");
    //        return twitterService.getTweets(tweetQuery.getCollectedBy(), tweetQuery
    //        .getCollectedOn());
    //    }
    //
    @GetMapping(path = "/tweets/paging")
    public Page<TweetResponseBody> getTweets(@RequestParam String botExecutionId,
                                 @RequestParam(defaultValue = "0") int page,
                                 @RequestParam(defaultValue = "20") int size) {
        logRequest("/tweets/paging");
        return twitterService.getTweets(botExecutionId, PageRequest.of(page, size))
                             .map(Tweet::toTweetResponseBody);
    }

    //
    //    @RequestMapping(value = "/tweets/export", method = RequestMethod.GET)
    //    public void exportTweetsAsCsv(
    //            @RequestParam(name = "after", required = false) @DateTimeFormat(pattern = "dd
    //            .MM" +
    //                    ".yyyy") Date after,
    //            @RequestParam(name = "botConfigId", required = false) Long botConfigId,
    //            HttpServletResponse response) {
    //        logRequest("/tweets/export");
    //        if (after == null && botConfigId == null) {
    //            throw new MissingRequestParamsException(
    //                    "At least one of the following request parameters have to be provided: " +
    //                            "'after', 'botConfigId'");
    //        }
    //        twitterService.writeCsvToResponse(twitterService.getTweets(after, botConfigId),
    //        response);
    //    }
    //
    @GetMapping(path = "/twitter-bot-statistics")
    public List<TwitterBotStatistics> getTwitterBotStatistics(@RequestParam String botConfigId) {
        logRequest("/twitter-bot-statistics");
        return twitterService.getTwitterBotStatistics(botConfigId);
    }

    private void logRequest(String endpoint) {
        try {
            Authentication authentication = SecurityContextHolder.getContext()
                                                                 .getAuthentication();
            logger.info("Incoming request to endpoint {} from user {}", endpoint,
                        authentication.getName());
        } catch (Exception e) {
            logger.warn("Failed to log tweet request data.");
        }
    }
}
