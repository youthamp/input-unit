package app.bot.platforms.instagram.rest.dto;

import app.bot.platforms.instagram.domain.InstagramUser;
import app.bot.platforms.shared.sentiment.rest.SentimentResponseBody;
import app.bot.platforms.shared.sentiment.rest.SentimentStatisticsResponseBody;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostResponseBody {
    private String id;
    private Date captionCreatedOn;
    private int commentCount;
    private int likeCount;
    private String captionText;
    private String contentType;
    private InstagramUser instagramUser;
    private boolean collectedFromNotifications;
    private SentimentResponseBody sentiment;
}
