package app.bot.platforms.instagram;

import app.bot.core.services.Bot;
import app.bot.platforms.BotGroup;
import app.bot.platforms.BotKey;
import app.bot.platforms.instagram.domain.InstagramUser;
import app.bot.platforms.instagram.domain.Post;
import app.bot.platforms.instagram.repository.InstagramPostRepository;
import app.bot.platforms.instagram.service.InstagramService;
import com.github.instagram4j.instagram4j.IGClient;
import com.github.instagram4j.instagram4j.actions.users.UserAction;
import com.github.instagram4j.instagram4j.exceptions.IGLoginException;
import com.github.instagram4j.instagram4j.models.media.UserTags;
import com.github.instagram4j.instagram4j.models.media.timeline.Comment;
import com.github.instagram4j.instagram4j.models.media.timeline.TimelineMedia;
import com.github.instagram4j.instagram4j.models.news.NewsStory;
import com.github.instagram4j.instagram4j.models.user.User;
import com.github.instagram4j.instagram4j.requests.friendships.FriendshipsActionRequest;
import com.github.instagram4j.instagram4j.requests.news.NewsInboxRequest;
import com.github.instagram4j.instagram4j.responses.news.NewsInboxResponse;
import org.quartz.JobExecutionContext;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InstagramBot implements Bot {

    private final static Logger log = LoggerFactory.getLogger(InstagramBot.class);

    private IGClient igClient;

    private String instagramUsername;

    private final InstagramService instagramService;

    private final InstagramPostRepository instagramPostRepository;

    private final Environment environment;

    public InstagramBot(InstagramService instagramService,
                        InstagramPostRepository instagramPostRepository, Environment environment) {
        this.instagramService = instagramService;
        this.instagramPostRepository = instagramPostRepository;
        this.environment = environment;
    }

    @Override
    public String getKey() {
        return BotKey.INSTAGRAM.getValue();
    }

    @Override
    public String getGroup() {
        return BotGroup.SOCIAL_NETWORK.getValue();
    }

    @Override
    public void interrupt() throws UnableToInterruptJobException {

    }

    /**
     * Collect posts which appear on the timeline of the configured instagram account and merge with
     * the posts collected from the notifications tab. This is necessary because sometimes the
     * fulltext of the posts collected from the notification tab is missing. But it is also possible
     * that the post does not appear on the personal timeline, but only in the notifications tab.
     * In the notifications tab all meta data like 'likes', 'followers count' etc. are missing.
     * That is why they have to be merged if possible, to collect as much information as possible.
     */
    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        try {
            initInstagramClient();
            collectPostsFromNotificationsAndFollowCreators();
            collectPostsFromTimelineAndMerge();
        } catch (RuntimeException e) {
            throw new RuntimeException("Error executing bot: " + e.getMessage());
        }
    }

    private void collectPostsFromTimelineAndMerge() {
        log.info("Execute Instagram bot...");
        igClient.actions()
                .timeline()
                .feed()
                .stream()
                .forEach(res -> {
                    List<TimelineMedia> items = res.getFeed_items();
                    items.forEach(timelinePost -> {
                        if (isProcessableItem(timelinePost)) {

                            instagramPostRepository.findById(Post.createIdFrom(timelinePost))
                                                   .ifPresentOrElse(post -> {
                                                       // update post with missing meta data
                                                       if (post.hasBeenCollectedFromNotifications()) {
                                                           instagramService.updatePost(post,
                                                                                       timelinePost);
                                                       }
                                                   }, () -> instagramService.savePost(Post.builder()
                                                                                          .fromTimelinePost(
                                                                                                  timelinePost)
                                                                                          .build()));
                        }
                    });
                });
    }

    private boolean isProcessableItem(TimelineMedia item) {
        return isProcessableCaption(item.getCaption()) &&
                (configuredAccountIsTagged(item.getUsertags()) || configuredAccountHasBeenMentioned(
                        item.getCaption()
                            .getText()));
    }

    private boolean configuredAccountIsTagged(UserTags userTags) {
        try {
            return userTags != null && containsPrivateAccount(userTags);
        } catch (RuntimeException e) {
            log.error("Failed to check if private instagram account has been tagged: {}",
                      e.getMessage());
            return false;
        }
    }

    private boolean containsPrivateAccount(UserTags userTags) {
        return userTags.getIn() != null && userTags.getIn()
                                                   .stream()
                                                   .anyMatch(userTag -> userTag.getUser()
                                                                               .getUsername()
                                                                               .equals(this.instagramUsername));
    }

    private boolean isProcessableCaption(Comment.Caption caption) {
        try {
            return caption != null && !caption.getText()
                                              .isBlank();
        } catch (RuntimeException e) {
            log.error("Failed to check if instagram caption is processable: {}", e.getMessage());
            return false;
        }
    }

    private void initInstagramClient() {
        try {
            this.instagramUsername = environment.getProperty("instagram.username", "");
            String password = environment.getProperty("instagram.password", "");
            this.igClient = IGClient.builder()
                                    .username(this.instagramUsername)
                                    .password(password)
                                    .login();
        } catch (IGLoginException e) {
            String msg = String.format("Failed to initialize instagram client: %s", e.getMessage());
            log.error(msg);
            throw new RuntimeException(msg);
        }
    }

    /**
     * Collect posts from the notification tab if the configured account has been tagged or
     * mentioned. Sometimes the text is not visible at this point. The creator will also be followed
     * by the configured instagram account. It is necessary to follow these people in order to get
     * the full post or at least additional meta data from the same post appearing on the personal
     * timeline.
     */
    private void collectPostsFromNotificationsAndFollowCreators() {
        NewsInboxResponse response = new NewsInboxRequest().execute(igClient)
                                                           .join();
        response.getOld_stories()
                .forEach(story -> {
                    if (story.getType() == 1) {
                        if (isProcessablePost(story)) {
                            try {
                                if (!instagramService.postAlreadyExists(story)) {
                                    User user = getAndFollowCreator(story);
                                    Post post = Post.builder()
                                                    .fromNotificationPost(story)
                                                    .instagramUser(InstagramUser.builder()
                                                                                .fromUser(user)
                                                                                .build())
                                                    .build();
                                    instagramService.savePost(post);

                                }
                            } catch (RuntimeException e) {
                                log.warn(String.format(
                                        "Failed to collect post and/or follow instagram user: %s ",
                                        e.getMessage()));
                            }

                        }
                    }
                });
    }

    private User getAndFollowCreator(NewsStory story) {
        String profileName = InstagramService.extractCreator(story);
        UserAction userAction = igClient.actions()
                                        .users()
                                        .findByUsername(profileName)
                                        .join();
        userAction.action(FriendshipsActionRequest.FriendshipsAction.CREATE)
                  .join();
        return userAction.getUser();
    }

    /**
     * Check if the configured instagram account has been mentioned or tagged in the story and
     * if it contains more text then only the 'tagged' or 'mentioned' notification
     *
     * @param newsStory Potential instagram post which appears in the notification tab.
     * @return True if the story is processable, false otherwise
     */
    private boolean isProcessablePost(NewsStory newsStory) {
        try {
            InstagramService.extractPostText(newsStory);
            return true;
        } catch (RuntimeException e) {
            log.debug("Post is not processable: {}", e.getMessage());
            return false;
        }
    }

    private boolean configuredAccountHasBeenMentioned(String postText) {
        try {
            return postText != null && postText.contains("mentioned you in a post");
        } catch (RuntimeException e) {
            log.warn(String.format(
                    "Failed to check if the configured instagram account has been mentioned in " +
                            "the " + "post: %s", e.getMessage()));
            return false;
        }
    }
}
