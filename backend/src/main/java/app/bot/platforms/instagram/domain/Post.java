package app.bot.platforms.instagram.domain;

import app.bot.platforms.EncryptionService;
import app.bot.platforms.instagram.rest.dto.PostResponseBody;
import app.bot.platforms.instagram.service.InstagramService;
import app.bot.platforms.shared.sentiment.domain.GoogleSentiment;
import app.bot.platforms.shared.sentiment.rest.SentimentResponseBody;
import com.github.instagram4j.instagram4j.models.media.timeline.TimelineMedia;
import com.github.instagram4j.instagram4j.models.news.NewsStory;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "instagram_post")
public class Post {

    @Id
    private String id;

    // milliseconds
    @Column(name = "post_created_on")
    private long postCreatedOn;

    @Column(name = "view_count")
    private int commentCount;

    @Column(name = "like_count")
    private int likeCount;

    @Column(name = "caption_text", length = 1000)
    private String captionText;

    @Column(name = "content_type")
    private String contentType;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private InstagramUser instagramUser;

    @UpdateTimestamp
    @Column(name = "modified")
    private Timestamp modified;

    @Column(name = "collected_from_notifications")
    private boolean collectedFromNotifications;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sentiment_id")
    private GoogleSentiment sentiment;

    /**
     * Create a unique id based on the encrypted value of the concatenated username + creation
     * timestamp of the {@link TimelineMedia}. The timestamp will first be converted into minutes.
     * This is necessary because of the inconsistency of timestamps between notifications and
     * timeline items.
     * <p>
     * Note: ids created from {@link NewsStory} and {@link TimelineMedia} should always be identical
     *
     * @param timelinePost The timeline media object representing the post
     * @return The unique id
     */
    public static String createIdFrom(TimelineMedia timelinePost) {
        return createIdFrom(timelinePost.getUser()
                                        .getUsername(), timelinePost.getCaption()
                                                                    .getCreated_at());
    }

    /**
     * Create a unique id based on the encrypted value of the concatenated username + creation
     * timestamp of the {@link NewsStory}. The timestamp will first be converted into minutes.
     * This is necessary because of the inconsistency of timestamps between notifications and
     * timeline items.
     * <p>
     * Note: ids created from {@link NewsStory} and {@link TimelineMedia} should always be identical
     *
     * @param notificationPost The post object which appears in the notification tab
     * @return The unique id
     */
    public static String createIdFrom(NewsStory notificationPost) {
        return createIdFrom(InstagramService.extractCreator(notificationPost),
                            InstagramService.extractTimestamp(notificationPost));
    }

    private static String createIdFrom(String username, double timestamp) {
        return createIdFrom(username, (long) timestamp);
    }

    private static String createIdFrom(String username, long timestamp) {
        long timestampInSeconds = timestamp / 60;
        return EncryptionService.encrypt(username + timestampInSeconds);
    }

    public static String createIdFromText(String text) {
        return EncryptionService.encrypt(getRelevantText(text));
    }

    public static String getRelevantText(String text) {
        if (text.contains("mentioned you:")) {
            return text.split("mentioned you:")[1];
        }
        return text;
    }

    public Post() {

    }

    public Post(String id, long postCreatedOn, int commentCount, int likeCount,
                String captionText, String contentType, InstagramUser instagramUser, boolean collectedFromNotifications) {
        this.id = id;
        this.postCreatedOn = postCreatedOn;
        this.commentCount = commentCount;
        this.likeCount = likeCount;
        this.captionText = captionText;
        this.contentType = contentType;
        this.instagramUser = instagramUser;
        this.collectedFromNotifications = collectedFromNotifications;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public String getCaptionText() {
        return captionText;
    }

    public String getContentType() {
        return contentType;
    }

    public InstagramUser getInstagramUser() {
        return instagramUser;
    }

    public Timestamp getModified() {
        return modified;
    }

    public void setModified(Timestamp modified) {
        this.modified = modified;
    }

    public boolean hasBeenCollectedFromNotifications(){
        return collectedFromNotifications;
    }

    public PostResponseBody toPostResponseBody() {

        var sentimentResponseBody = (sentiment != null) ? SentimentResponseBody.builder()
                                                                               .id(sentiment.getId())
                                                                               .score(sentiment.getScore())
                                                                               .magnitude(sentiment.getMagnitude())
                                                                               .build() : null;
        return PostResponseBody.builder()
                               .id(this.id)
                               .captionCreatedOn(new Date(postCreatedOn))
                               .captionText(this.captionText)
                               .commentCount(this.commentCount)
                               .contentType(this.contentType)
                               .likeCount(this.likeCount)
                               .instagramUser(this.instagramUser)
                               .collectedFromNotifications(this.collectedFromNotifications)
                               .sentiment(sentimentResponseBody)
                               .build();
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPostCreatedOn(long postCreatedOn) {
        this.postCreatedOn = postCreatedOn;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public void setCaptionText(String captionText) {
        this.captionText = captionText;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setInstagramUser(InstagramUser instagramUser) {
        this.instagramUser = instagramUser;
    }

    public void setCollectedFromNotifications(boolean collectedFromNotifications) {
        this.collectedFromNotifications = collectedFromNotifications;
    }

    public static class Builder {
        private String id;
        private long postCreatedOn;
        private int commentCount;
        private int likeCount;
        private String captionText;
        private String contentType;
        private InstagramUser instagramUser;
        private boolean collectedFromNotifications;

        Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder captionCreatedOn(long postCreatedOn) {
            this.postCreatedOn = postCreatedOn;
            return this;
        }

        public Builder commentCount(int commentCount) {
            this.commentCount = commentCount;
            return this;
        }

        public Builder likeCount(int likeCount) {
            this.likeCount = likeCount;
            return this;
        }

        public Builder captionText(String captionText) {
            this.captionText = captionText;
            return this;
        }

        public Builder contentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public Builder instagramUser(InstagramUser instagramUser) {
            this.instagramUser = instagramUser;
            return this;
        }

        public Builder collectedFromNotifications(boolean collectedFromNotifications) {
            this.collectedFromNotifications = collectedFromNotifications;
            return this;
        }

        public Builder fromTimelinePost(TimelineMedia timelineMedia) {

            this.postCreatedOn = timelineMedia.getCaption()
                                              .getCreated_at() * 1000;
            this.likeCount = timelineMedia.getLike_count();
            this.commentCount = timelineMedia.getComment_count();
            this.captionText = getRelevantText(timelineMedia.getCaption()
                                                            .getText());
            this.id = createIdFrom(timelineMedia);
            this.contentType = timelineMedia.getCaption()
                                            .getContent_type();
            this.instagramUser = InstagramUser.builder()
                                              .fromUser(timelineMedia.getUser())
                                              .build();
            this.collectedFromNotifications = false;
            return this;
        }

        public Builder fromNotificationPost(NewsStory notificationPost) {
            this.id = createIdFrom(notificationPost);
            this.captionText = InstagramService.extractPostText(notificationPost);
            this.postCreatedOn = (long)InstagramService.extractTimestamp(notificationPost) * 1000;
            this.collectedFromNotifications = true;
            return this;
        }

        public Post build() {
            return new Post(id, postCreatedOn, commentCount, likeCount, captionText, contentType,
                            instagramUser, collectedFromNotifications);
        }
    }
}
