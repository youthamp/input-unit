package app.bot.platforms.instagram.service;

import app.bot.platforms.instagram.domain.Post;
import app.bot.platforms.instagram.repository.InstagramUserRepository;
import app.bot.platforms.instagram.repository.InstagramPostRepository;
import com.github.instagram4j.instagram4j.models.media.timeline.TimelineMedia;
import com.github.instagram4j.instagram4j.models.news.NewsStory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class InstagramService {

    private final static Logger log = LoggerFactory.getLogger(InstagramService.class);

    private final InstagramPostRepository instagramPostRepository;
    private final InstagramUserRepository instagramUserRepository;

    @Autowired
    public InstagramService(InstagramPostRepository instagramPostRepository,
                            InstagramUserRepository instagramUserRepository) {
        this.instagramPostRepository = instagramPostRepository;
        this.instagramUserRepository = instagramUserRepository;
    }

    public Optional<Post> savePost(Post post) {
        try {
            if (!instagramUserRepository.existsById(post.getInstagramUser()
                                                        .getId())) {
                instagramUserRepository.save(post.getInstagramUser());
            }
            return Optional.of(this.instagramPostRepository.save(post));
        } catch (RuntimeException e) {
            log.error("Error persisting instagram timeline item: {}", e.getMessage());
            return Optional.empty();
        }
    }

    public List<Post> getPosts(Date after) {
        try {
            return instagramPostRepository.findAllAfter(after.getTime());
        } catch (RuntimeException e) {
            log.error("Error retrieving timeline items after the given date: {}", e.getMessage());
            return Collections.emptyList();
        }
    }

    public boolean postAlreadyExists(TimelineMedia timelinePost) {
        try {
            return instagramPostRepository.existsById(Post.createIdFrom(timelinePost));
        } catch (RuntimeException e) {
            return false;
        }
    }

    public boolean postAlreadyExists(NewsStory newsStory) {
        try {
            return instagramPostRepository.existsById(Post.createIdFrom(newsStory));
        } catch (RuntimeException e) {
            return false;
        }
    }

    public Optional<Post> updatePost(Post post, TimelineMedia timelinePost) {
        try{
            Post postWithAdditionalData = Post.builder().fromTimelinePost(timelinePost).build();
            if(!post.getId().equals(Post.createIdFrom(timelinePost))){
                throw new RuntimeException("The two posts do not have the same id!");
            }

            post.setContentType(postWithAdditionalData.getContentType());
            post.setCaptionText(postWithAdditionalData.getCaptionText());
            post.setCollectedFromNotifications(false);
            post.setCommentCount(postWithAdditionalData.getCommentCount());
            post.setLikeCount(postWithAdditionalData.getLikeCount());
            post.setInstagramUser(postWithAdditionalData.getInstagramUser());
            return savePost(post);
        }catch (RuntimeException e){
            log.error("Failed to update instagram post: {}", e.getMessage());
            return Optional.empty();
        }
    }
    /**
     * Extract the profile name of the creator of the post
     */
    @SuppressWarnings("unchecked")
    public static String extractCreator(NewsStory story) {
        return ((LinkedHashMap<String, String>) story.getExtraProperties()
                                                     .get("args")).get("profile_name");

    }

    /**
     * Extract the text from the story
     */
    @SuppressWarnings("unchecked")
    public static String extractPostText(NewsStory story) {
        return ((LinkedHashMap<String, String>) story.getExtraProperties()
                                                     .get("args")).get("text").split("you in a post:")[1];

    }

    /**
     * Extract the profile name of the creator of the post
     */
    @SuppressWarnings("unchecked")
    public static double extractTimestamp(NewsStory story) {
        return (double) ((Map<String, Object>) story.getExtraProperties()
                                                    .get("args")).get("timestamp");

    }
}
