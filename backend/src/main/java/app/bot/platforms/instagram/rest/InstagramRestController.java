package app.bot.platforms.instagram.rest;

import app.bot.platforms.instagram.domain.Post;
import app.bot.platforms.instagram.rest.dto.PostResponseBody;
import app.bot.platforms.instagram.service.InstagramService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static app.system.rest.Constants.Web.INSTAGRAM_DATA_URL;

@RestController
@RequestMapping(value = INSTAGRAM_DATA_URL)
public class InstagramRestController {

    private final InstagramService instagramService;

    public InstagramRestController(InstagramService instagramService) {
        this.instagramService = instagramService;
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET, produces =
            "application/json")
    public List<PostResponseBody> getTimelineItems(
            @RequestParam(name = "after") @DateTimeFormat(pattern = "dd.MM.yyyy") Date after) {

        return instagramService.getPosts(after)
                               .stream()
                               .map(Post::toPostResponseBody)
                               .collect(Collectors.toList());
    }

}