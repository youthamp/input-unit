package app.bot.platforms.instagram.repository;

import app.bot.platforms.instagram.domain.InstagramUser;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InstagramUserRepository extends PagingAndSortingRepository<InstagramUser, String> {
}