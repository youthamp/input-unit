package app.bot.platforms.instagram.repository;

import app.bot.platforms.instagram.domain.Post;
import app.bot.platforms.twitter.domain.Tweet;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface InstagramPostRepository extends PagingAndSortingRepository<Post, String> {
    List<Post> findAll();

    @Query(value="SELECT * FROM instagram_post timeline_item WHERE timeline_item.post_created_on > :after", nativeQuery = true)
    List<Post> findAllAfter(Long after);

    List<Post> findAllBySentimentIdIsNull();

    List<Post> findAllBySentimentIdIsNotNullAndCaptionTextContains(String searchTerm);
}