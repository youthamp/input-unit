package app.bot.platforms.instagram.domain;

import app.bot.platforms.EncryptionService;
import com.github.instagram4j.instagram4j.models.user.User;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "instagram_user")
public class InstagramUser {

    @Id
    private String id;

    @Column(name = "media_count")
    private int mediaCount;

    @Column(name = "follower_count")
    private int followerCount;

    @Column(name = "following_count")
    private int followingCount;

    @Column(name = "is_business")
    private boolean isBusiness;

    @Column(name = "is_verified")
    private boolean isVerified;

    @UpdateTimestamp
    @Column(name = "modified")
    private Timestamp modified;

    @CreationTimestamp
    @Column(name = "created")
    private Timestamp created;

    public InstagramUser(){

    }

    public InstagramUser(String id, int mediaCount, int followerCount, int followingCount,
                         boolean isBusiness, boolean isVerified) {
        this.id = id;
        this.mediaCount = mediaCount;
        this.followerCount = followerCount;
        this.followingCount = followingCount;
        this.isBusiness = isBusiness;
        this.isVerified = isVerified;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public int getMediaCount() {
        return mediaCount;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public int getFollowingCount() {
        return followingCount;
    }

    public boolean isBusiness() {
        return isBusiness;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public Timestamp getModified() {
        return modified;
    }

    public Timestamp getCreated() {
        return created;
    }

    public static class Builder {
        private String id;
        private int mediaCount;
        private int followerCount;
        private int followingCount;
        private boolean isBusiness;
        private boolean isVerified;

        Builder() {
        }

        public Builder id(String id) {
            this.id = EncryptionService.encrypt(id);;
            return this;
        }

        public Builder mediaCount(int mediaCount) {
            this.mediaCount = mediaCount;
            return this;
        }

        public Builder followerCount(int followerCount) {
            this.followerCount = followerCount;
            return this;
        }

        public Builder followingCount(int followingCount) {
            this.followingCount = followingCount;
            return this;
        }

        public Builder isBusiness(boolean isBusiness) {
            this.isBusiness = isBusiness;
            return this;
        }

        public Builder isVerified(boolean isVerified) {
            this.isVerified = isVerified;
            return this;
        }

        public Builder fromUser(User user) {
            this.id = EncryptionService.encrypt(user.getPk());
            this.mediaCount = user.getMedia_count();
            this.followerCount = user.getFollower_count();
            this.followingCount = user.getFollowing_count();
            this.isBusiness = user.is_business();
            this.isVerified = user.is_verified();
            return this;
        }

        public InstagramUser build() {
            return new InstagramUser(id, mediaCount, followerCount, followingCount, isBusiness,
                                     isVerified);
        }
    }
}
