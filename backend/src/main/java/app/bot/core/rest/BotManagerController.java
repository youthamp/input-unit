package app.bot.core.rest;

import app.bot.core.services.BotConfigService;
import app.bot.core.services.BotManager;
import app.system.rest.dto.ApiResponse;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static app.system.rest.Constants.Web.BOT_MANAGER_URL;


@RestController
@RequestMapping(path = BOT_MANAGER_URL)
public class BotManagerController {

    private final BotManager botManager;

    private final BotConfigService botConfigService;

    private final BotDetailsDtoService botDetailsDtoService;

    public BotManagerController(BotManager botManager, BotConfigService botConfigService,
                                BotDetailsDtoService botDetailsDtoService) {
        this.botManager = botManager;
        this.botConfigService = botConfigService;
        this.botDetailsDtoService = botDetailsDtoService;
    }

    @PostMapping(path = "/configuration", consumes = "application/json", produces = "application/json")
    public ApiResponse createBot(@RequestBody BotConfigRequestBody botConfigRequestBody) {
        try {
            return new ApiResponse.Builder().ok(
                    botDetailsDtoService.transform(botManager.createBot(botConfigRequestBody)))
                                            .build();
        } catch (RuntimeException e) {
            return new ApiResponse.Builder().error(e.getMessage())
                                            .build();
        }
    }

    @GetMapping(path = "/details")
    public ApiResponse getAllBotDetails() {
        try {
            List<BotInfoResponseBody> botConfigs =
                    botDetailsDtoService.transform(botConfigService.getAll(false));
            return new ApiResponse.Builder().ok(botConfigs)
                                            .build();
        } catch (RuntimeException e) {
            return new ApiResponse.Builder().error(e.getMessage())
                                            .build();
        }
    }

    @GetMapping(path = "/details/{botConfigId}")
    public ApiResponse getBotDetails(@PathVariable String botConfigId) {
        try {
            return botConfigService.find(botConfigId)
                                   .map(botConfig -> new ApiResponse.Builder().ok(
                                           botDetailsDtoService.transform(botConfig))
                                                                              .build())
                                   .orElseThrow(() -> new RuntimeException(String.format(
                                           "Failed to get bot details for bot config %s",
                                           botConfigId)));

        } catch (RuntimeException e) {
            return new ApiResponse.Builder().error(e.getMessage())
                                            .build();
        }
    }

    @DeleteMapping(path = "/configuration")
    public ApiResponse deleteBotConfig(String botConfigId) {
        if (botManager.deleteBot(botConfigId)) {
            return new ApiResponse.Builder().ok("Successful deleted bot")
                                            .build();
        } else {
            return new ApiResponse.Builder().notFound(
                    String.format("Could not find entity with id '%s'", botConfigId))
                                            .build();
        }
    }

    @GetMapping(path = "/configuration/deactivate")
    public ApiResponse deactivateConfig(@RequestParam String botConfigId) {
        if (botManager.deactivateBot(botConfigId)) {
            return new ApiResponse.Builder().ok("Successful deactivated bot configuration")
                                            .build();
        } else {
            return new ApiResponse.Builder().error("Failed to deactivate bot configuration")
                                            .build();
        }
    }

    @GetMapping(path = "/configuration/activate")
    public ApiResponse activateConfig(@RequestParam String botConfigId) {
        if (botManager.activateBot(botConfigId)) {
            return new ApiResponse.Builder().ok("Successful activated bot configuration")
                                            .build();
        } else {
            return new ApiResponse.Builder().error("Failed to activate bot configuration")
                                            .build();
        }
    }

    @GetMapping(path = "/configuration/execute/{botConfigId}")
    public ApiResponse executeManually(@PathVariable String botConfigId) {
        if (botManager.executeBotManually(botConfigId)) {
            return new ApiResponse.Builder().ok("Successful executed bot")
                                            .build();
        } else {
            return new ApiResponse.Builder().error("Failed to execute bot")
                                            .build();
        }
    }

    @GetMapping(path = "/configuration/archive")
    public ApiResponse archiveBot(@RequestParam String botConfigId) {
        if (botManager.archiveBotConfig(botConfigId)) {
            return new ApiResponse.Builder().ok("Successful archived bot")
                                            .build();
        } else {
            return new ApiResponse.Builder().error("Failed to archive bot")
                                            .build();
        }
    }
}
