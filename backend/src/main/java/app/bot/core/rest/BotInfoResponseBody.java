package app.bot.core.rest;

import java.sql.Timestamp;
import java.util.Map;

public class BotInfoResponseBody {

    private String id;

    private String botKey;

    private String cron;

    private int executionCount;

    private Timestamp lastExecution;

    private boolean lastExecutionError;

    private String errorMsg;

    private boolean manualExecution;

    private boolean active;

    private Map<String, String> properties;

    public BotInfoResponseBody() {
    }

    public BotInfoResponseBody(String id, String botKey, String cron, Timestamp lastExecution,
                               boolean lastExecutionError, String errorMsg,
                               boolean manualExecution, boolean active,
                               Map<String, String> properties, int executionCount) {
        this.id = id;
        this.botKey = botKey;
        this.cron = cron;
        this.lastExecution = lastExecution;
        this.lastExecutionError = lastExecutionError;
        this.errorMsg = errorMsg;
        this.manualExecution = manualExecution;
        this.active = active;
        this.properties = properties;
        this.executionCount = executionCount;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return this.id;
    }

    public String getBotKey() {
        return this.botKey;
    }

    public String getCron() {
        return this.cron;
    }

    public boolean isManualExecution() {
        return this.manualExecution;
    }

    public boolean isActive() {
        return this.active;
    }

    public Map<String, String> getProperties() {
        return this.properties;
    }

    public int getExecutionCount() {
        return this.executionCount;
    }

    public Timestamp getLastExecution() {
        return this.lastExecution;
    }

    public boolean isLastExecutionError() {
        return this.lastExecutionError;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }


    public static class Builder {
        private String id;
        private String botKey;
        private String cron;
        private Timestamp lastExecution;
        private boolean lastExecutionError;
        private String errorMsg;
        private boolean manualExecution;
        private boolean active;
        private Map<String, String> properties;
        private int executionCount;

        Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder botKey(String botKey) {
            this.botKey = botKey;
            return this;
        }

        public Builder cron(String cron) {
            this.cron = cron;
            return this;
        }

        public Builder lastExecution(Timestamp lastExecution) {
            this.lastExecution = lastExecution;
            return this;
        }

        public Builder lastExecutionError(boolean lastExecutionError) {
            this.lastExecutionError = lastExecutionError;
            return this;
        }

        public Builder errorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
            return this;
        }

        public Builder manualExecution(boolean manualExecution) {
            this.manualExecution = manualExecution;
            return this;
        }

        public Builder active(boolean active) {
            this.active = active;
            return this;
        }

        public Builder properties(Map<String, String> properties) {
            this.properties = properties;
            return this;
        }


        public Builder executionCount(int executionCount) {
            this.executionCount = executionCount;
            return this;
        }

        public BotInfoResponseBody build() {
            return new BotInfoResponseBody(id, botKey, cron, lastExecution, lastExecutionError,
                                           errorMsg, manualExecution, active, properties, executionCount);
        }
    }
}
