package app.bot.core.rest;

import app.bot.core.services.BotExecutionService;
import app.bot.core.domain.BotConfig;
import app.bot.core.domain.BotExecution;
import app.bot.core.domain.BotSchedule;
import app.bot.core.repositories.BotExecutionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BotDetailsDtoService {

    private final BotExecutionService botExecutionService;
    private final BotExecutionRepository botExecutionRepository;

    public BotDetailsDtoService(BotExecutionService botExecutionService,
                                BotExecutionRepository botExecutionRepository) {
        this.botExecutionService = botExecutionService;
        this.botExecutionRepository = botExecutionRepository;
    }

    public BotInfoResponseBody transform(BotConfig botConfig) {
        BotSchedule botSchedule = botConfig.getBotSchedule();

        BotInfoResponseBody.Builder builder =
                new BotInfoResponseBody.Builder().id(botConfig.getId())
                                                 .botKey(botConfig.getBotKey())
                                                 .cron(botSchedule.getCron())
                                                 .executionCount(
                                                         botExecutionService.countBotExecutions(
                                                                 botConfig.getId()))
                                                 .properties(botConfig.getProperties())
                                                 .manualExecution(botSchedule.isManualExecution())
                                                 .active(botConfig.isActive());
        botExecutionRepository.findFirstByBotConfigIdOrderByExecutionEndDesc(botConfig.getId())
                              .ifPresent(botExecution -> {
                                  builder.lastExecution(botExecution.getExecutionEnd());
                                  builder.lastExecutionError(botExecution.getStatus()
                                                                         .equals(BotExecution.Status.ERROR));
                                  builder.errorMsg(botExecution.getMessage());

                              });
        return builder.build();
    }

    public List<BotInfoResponseBody> transform(List<BotConfig> botConfigs) {
        return botConfigs.stream()
                         .map(this::transform)
                         .collect(Collectors.toList());
    }
}
