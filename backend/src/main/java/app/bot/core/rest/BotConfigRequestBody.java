package app.bot.core.rest;

import java.util.Map;

public class BotConfigRequestBody {

    private String botKey;

    private String botGroup;

    private String cron;

    private boolean manualExecution;

    private Map<String, String> properties;

    public BotConfigRequestBody() {
    }

    public String getBotKey() {
        return this.botKey;
    }

    public String getBotGroup() {
        return this.botGroup;
    }

    public String getCron() {
        return this.cron;
    }

    public boolean isManualExecution() {
        return this.manualExecution;
    }

    public Map<String, String> getProperties() {
        return this.properties;
    }
}
