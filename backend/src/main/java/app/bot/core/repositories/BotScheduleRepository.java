package app.bot.core.repositories;

import app.bot.core.domain.BotSchedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BotScheduleRepository extends JpaRepository<BotSchedule, String> {
}
