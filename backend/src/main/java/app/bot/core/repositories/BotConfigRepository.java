package app.bot.core.repositories;

import app.bot.core.domain.BotConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BotConfigRepository extends JpaRepository<BotConfig, String> {

    @Query(value = "SELECT * FROM bot_config WHERE archived = :archived", nativeQuery = true)
    List<BotConfig> findAllByArchived(boolean archived);
}
