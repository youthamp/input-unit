package app.bot.core.repositories;

import app.bot.core.domain.BotExecution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface BotExecutionRepository extends JpaRepository<BotExecution, String> {

    @Query(value = "SELECT * FROM bot_execution botExecution WHERE botExecution.bot_config_id = :botConfigId", nativeQuery = true)
    List<BotExecution> findAllByBotConfigId(String botConfigId);

    int countByBotConfigId(String botConfigId);

    Optional<BotExecution> findFirstByBotConfigIdOrderByExecutionEndDesc(String botConfigId);
}
