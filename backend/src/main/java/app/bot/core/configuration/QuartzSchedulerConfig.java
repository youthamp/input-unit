package app.bot.core.configuration;

import app.bot.core.listeners.BotExecutionListener;
import org.quartz.ListenerManager;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import static org.quartz.impl.matchers.GroupMatcher.anyJobGroup;

/**
 * Global configuration component for the {@link Scheduler}
 */
@Configuration
public class QuartzSchedulerConfig {

    private final SchedulerFactoryBean factory;
    private final BotExecutionListener botExecutionListener;

    public QuartzSchedulerConfig(SchedulerFactoryBean factory,
                                 BotExecutionListener botExecutionListener) {
        this.factory = factory;
        this.botExecutionListener = botExecutionListener;
    }

    @Bean
    public Scheduler scheduler() throws SchedulerException {
        Scheduler scheduler = factory.getScheduler();
        ListenerManager listenerManager = scheduler.getListenerManager();
        listenerManager.addJobListener(botExecutionListener, anyJobGroup());
        scheduler.start();
        return scheduler;
    }
}
