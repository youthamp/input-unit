package app.bot.core.domain;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "bot_schedule")
public class BotSchedule {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    private String id;

    @Column(name = "manual_execution")
    private boolean manualExecution;

    @Column(name = "cron")
    private String cron;

    @UpdateTimestamp
    @Column
    private Timestamp modified;

    @CreationTimestamp
    @Column
    private Timestamp created;

    public BotSchedule(boolean manualExecution, String cron) {
        this.manualExecution = manualExecution;
        this.cron = cron;
    }

    public BotSchedule() {

    }

    public String getId() {
        return this.id;
    }

    public String getCron() {
        return this.cron;
    }

    public void setId(String name) {
        this.id = name;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public boolean isManualExecution() {
        return manualExecution;
    }

    public void setManualExecution(boolean manualExecution) {
        this.manualExecution = manualExecution;
    }

    public Timestamp getModified() {
        return modified;
    }

    public Timestamp getCreated() {
        return created;
    }

    public static final class Builder {

        private boolean manualExecution;
        private String cron;
        private Long startDate;
        private Long endDate;

        public Builder setManualExecution(boolean manualExecution) {
            this.manualExecution = manualExecution;
            return this;
        }

        public Builder setCron(String cron) {
            this.cron = cron;
            return this;
        }

        public Builder setStartDate(Long startDate) {
            this.startDate = startDate;
            return this;
        }

        public Builder setEndDate(Long endDate) {
            this.endDate = endDate;
            return this;
        }

        public BotSchedule build() {
            return new BotSchedule(manualExecution, cron);
        }
    }
}
