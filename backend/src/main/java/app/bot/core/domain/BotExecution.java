package app.bot.core.domain;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "bot_execution")
public class BotExecution {

    @Id
    private String id;

    @Column(name = "status")
    private Status status;

    @Column(name = "message", length = 1000)
    private String message;

    @ManyToOne
    @JoinColumn(name = "bot_config_id")
    private BotConfig botConfig;

    @CreationTimestamp
    @Column(name = "execution_start")
    private Timestamp executionStart;

    @Column(name = "execution_end")
    private Timestamp executionEnd;

    public BotExecution() {

    }

    public BotExecution(String id, Status status, String message, BotConfig botConfig) {
        this.id = id;
        this.status = status;
        this.message = message;
        this.botConfig = botConfig;
    }

    public String getId() {
        return this.id;
    }

    public String getMessage() {
        return this.message;
    }

    public BotConfig getBotConfig() {
        return this.botConfig;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setBotConfig(BotConfig botConfig) {
        this.botConfig = botConfig;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Timestamp getExecutionStart() {
        return this.executionStart;
    }

    public Timestamp getExecutionEnd() {
        return this.executionEnd;
    }

    public void setExecutionStart(Timestamp executionStart) {
        this.executionStart = executionStart;
    }

    public void setExecutionEnd(Timestamp executionEnd) {
        this.executionEnd = executionEnd;
    }

    public static final class Builder {

        private String id;
        private Status status;
        private String message;
        private BotConfig botConfig;

        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        public Builder setStatus(Status status) {
            this.status = status;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setBotConfig(BotConfig botConfig) {
            this.botConfig = botConfig;
            return this;
        }

        public BotExecution build() {
            return new BotExecution(id, status, message, botConfig);
        }
    }

    public enum Status {
        RUNNING, FINISHED, ERROR
    }
}
