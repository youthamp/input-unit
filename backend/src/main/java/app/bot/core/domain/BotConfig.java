package app.bot.core.domain;


import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Map;

@Entity
@Table(name = "bot_config")
public class BotConfig {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "active")
    private boolean active;

    @Column(name = "archived")
    private boolean archived;

    @Column(name = "bot_key")
    private String botKey;

    @Column(name = "bot_group")
    private String botGroup;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "bot_schedule_id")
    private BotSchedule botSchedule;

    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, String> properties;

    @UpdateTimestamp
    @Column
    private Timestamp modified;

    @CreationTimestamp
    @Column
    private Timestamp created;

    public BotConfig(boolean active, String botKey, String botGroup, BotSchedule botSchedule,
                     Map<String, String> properties) {
        this.active = active;
        this.archived = false;
        this.botKey = botKey;
        this.botGroup = botGroup;
        this.botSchedule = botSchedule;
        this.properties = properties;
    }

    public BotConfig() {

    }

    public String getId() {
        return this.id;
    }

    public boolean isActive() {
        return this.active;
    }

    public BotSchedule getBotSchedule() {
        return this.botSchedule;
    }

    public Map<String, String> getProperties() {
        return this.properties;
    }


    public void setActive(boolean active) {
        this.active = active;
    }

    public void setBotSchedule(BotSchedule botSchedule) {
        this.botSchedule = botSchedule;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public String getBotKey() {
        return botKey;
    }

    public void setBotKey(String botKey) {
        this.botKey = botKey;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Timestamp getModified() {
        return modified;
    }

    public Timestamp getCreated() {
        return created;
    }

    public String getBotGroup() {
        return this.botGroup;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setBotGroup(String botGroup) {
        this.botGroup = botGroup;
    }

    public static final class Builder {

        private boolean active;
        private String botKey;
        private String botGroup;
        private BotSchedule botSchedule;
        private Map<String, String> properties;

        public Builder setActive(boolean active) {
            this.active = active;
            return this;
        }

        public Builder setBotKey(String botKey) {
            this.botKey = botKey;
            return this;
        }

        public Builder setBotGroup(String botGroup) {
            this.botGroup = botGroup;
            return this;
        }

        public Builder setBotSchedule(BotSchedule botSchedule) {
            this.botSchedule = botSchedule;
            return this;
        }

        public Builder setProperties(Map<String, String> properties) {
            this.properties = properties;
            return this;
        }

        public BotConfig build() {
            return new BotConfig(active, botKey, botGroup, botSchedule, properties);
        }
    }
}
