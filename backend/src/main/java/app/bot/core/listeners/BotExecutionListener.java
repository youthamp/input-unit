package app.bot.core.listeners;

import app.bot.core.domain.BotConfig;
import app.bot.core.domain.BotExecution;
import app.bot.core.services.JobExecutionKeys;
import app.bot.core.services.BotConfigService;
import app.bot.core.services.BotExecutionService;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

@Component
public class BotExecutionListener implements JobListener {

    private final static Logger log = LoggerFactory.getLogger(BotExecutionListener.class);

    public static final String LISTENER_NAME = "Bot execution listener";

    private final BotConfigService botConfigService;
    private final BotExecutionService botExecutionService;

    public BotExecutionListener(BotConfigService botConfigService,
                                BotExecutionService botExecutionService) {
        this.botConfigService = botConfigService;
        this.botExecutionService = botExecutionService;
    }

    @Override
    public String getName() {
        return LISTENER_NAME;
    }

    @Override
    public void jobToBeExecuted(JobExecutionContext context) {
        log.debug("Start bot execution: " + context.getJobDetail()
                                                   .toString());
        String botExecutionId = context.getFireInstanceId();
        context.put(JobExecutionKeys.BOT_EXECUTION_ID, botExecutionId);
        createActiveBotExecution(context);
    }

    @Override
    public void jobExecutionVetoed(JobExecutionContext context) {
        log.debug("Vetoed bot execution: " + context.getJobDetail()
                                                    .toString());
    }

    @Override
    public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
        log.debug("Finished bot execution: " + context.getJobDetail()
                                                      .toString());
        updateBotExecution(context, jobException).orElseThrow(
                () -> new RuntimeException("Failed to update bot execution."));

    }

    private void createActiveBotExecution(JobExecutionContext context) {
        JobDataMap jobDataMap = context.getJobDetail()
                                       .getJobDataMap();
        String botConfigId = jobDataMap.getString(JobExecutionKeys.BOT_CONFIG_ID);
        Optional<BotConfig> botConfigOptional = botConfigService.find(botConfigId);

        if (botConfigOptional.isPresent()) {
            BotConfig botConfig = botConfigOptional.get();
            String botExecutionId = (String) context.get(JobExecutionKeys.BOT_EXECUTION_ID);

            if (botExecutionId == null || botExecutionId.isEmpty()) {
                throw new RuntimeException(
                        "Failed to create active bot execution: bot execution id is null or empty" +
                                ".");
            }

            BotExecution botExecution = new BotExecution.Builder().setId(botExecutionId)
                                                                  .setBotConfig(botConfig)
                                                                  .setStatus(
                                                                          BotExecution.Status.RUNNING)
                                                                  .build();
            botExecutionService.create(botExecution);
        } else {
            log.error("Failed to create active bot execution instance: Could not find a " +
                              "corresponding bot config with id {}", botConfigId);
        }
    }

    private Optional<BotExecution> updateBotExecution(JobExecutionContext context,
                                                      JobExecutionException jobException) {

        String botExecutionId = (String) context.get(JobExecutionKeys.BOT_EXECUTION_ID);
        return botExecutionService.findById(botExecutionId)
                                  .map(botExecution -> {
                                      botExecution.setExecutionEnd(now());
                                      if (jobException != null) {
                                          botExecution.setMessage(getTruncatedErrorMessage(jobException));
                                          botExecution.setStatus(BotExecution.Status.ERROR);
                                      } else {
                                          botExecution.setStatus(BotExecution.Status.FINISHED);
                                      }

                                      return botExecutionService.update(botExecution)
                                                                .orElse(null);
                                  });
    }

    private Timestamp now(){
        return new Timestamp(new Date().getTime());
    }

    private String getTruncatedErrorMessage(JobExecutionException jobException) {
        return jobException.getMessage()
                           .substring(0, Math.min(jobException.getMessage()
                                                              .length(), 1000));
    }
}
