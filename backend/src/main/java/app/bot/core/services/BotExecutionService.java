package app.bot.core.services;

import app.bot.core.domain.BotExecution;

import java.util.List;
import java.util.Optional;

/**
 * @author Alexander Voigt
 * Service for managing {@link app.bot.core.domain.BotExecution}
 */
public interface BotExecutionService {

    /**
     * Get {@link BotExecution} by id
     *
     * @param botExecutionId The bot execution id
     * @return Optional of {@link BotExecution}
     */
    Optional<BotExecution> findById(String botExecutionId);

    /**
     * Get all {@link BotExecution}'s with the given bot config id
     *
     * @param botConfigId The bot execution id
     * @return Optional of {@link BotExecution}
     */
    List<BotExecution> findByBotConfigId(String botConfigId);

    /**
     * Create {@link BotExecution}
     *
     * @return Optional of {@link BotExecution}
     */
    Optional<BotExecution> create(BotExecution botExecution);

    /**
     * Update {@link BotExecution}
     *
     * @return Optional of {@link BotExecution}
     */
    Optional<BotExecution> update(BotExecution botExecution);

    /**
     * Delete all bot executions by bot config id
     *
     * @param botConfigId The bot config id
     */
    void deleteByBotConfigId(String botConfigId);

    /**
     * @param botConfigId The bot config id
     * @return The number of bot executions
     */
    int countBotExecutions(String botConfigId);

}
