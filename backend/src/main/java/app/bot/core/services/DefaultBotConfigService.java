package app.bot.core.services;

import app.bot.core.domain.BotConfig;
import app.bot.core.domain.BotSchedule;
import app.bot.core.repositories.BotConfigRepository;
import app.bot.core.repositories.BotScheduleRepository;
import app.bot.core.rest.BotConfigRequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DefaultBotConfigService implements BotConfigService {

    private final static Logger log = LoggerFactory.getLogger(DefaultBotConfigService.class);

    private final BotConfigRepository botConfigRepository;
    private final BotScheduleRepository botScheduleRepository;
    private final BotExecutionService botExecutionService;

    public DefaultBotConfigService(BotConfigRepository botConfigRepository,
                                   BotScheduleRepository botScheduleRepository,
                                   BotExecutionService botExecutionService) {
        this.botConfigRepository = botConfigRepository;
        this.botScheduleRepository = botScheduleRepository;
        this.botExecutionService = botExecutionService;
    }

    @Override
    public BotConfig create(BotConfigRequestBody requestBody) {
        try {
            BotSchedule botSchedule = createBotSchedule(requestBody);
            BotConfig botConfig = new BotConfig.Builder().setActive(!botSchedule.isManualExecution())
                                                         .setBotSchedule(botSchedule)
                                                         .setBotKey(requestBody.getBotKey())
                                                         .setBotGroup(requestBody.getBotGroup())
                                                         .setProperties(getNoneEmptyProperties(
                                                                 requestBody))
                                                         .setBotSchedule(botSchedule)
                                                         .build();
            botScheduleRepository.save(botSchedule);
            return botConfigRepository.save(botConfig);
        } catch (RuntimeException e) {
            throw new RuntimeException("Failed to create bot config: " + e.getMessage());
        }
    }

    @Override
    public BotConfig saveOrUpdateBotConfig(BotConfig botConfig) {
        return botConfigRepository.save(botConfig);
    }

    private BotSchedule createBotSchedule(BotConfigRequestBody requestBody) {
        return new BotSchedule.Builder().setManualExecution(requestBody.isManualExecution())
                                        .setCron(requestBody.getCron())
                                        .build();
    }

    private Map<String, String> getNoneEmptyProperties(BotConfigRequestBody botConfigRequestBody) {
        Map<String, String> cleanProperties = new HashMap<>();
        Map<String, String> properties = botConfigRequestBody.getProperties();
        botConfigRequestBody.getProperties()
                            .keySet()
                            .iterator()
                            .forEachRemaining(key -> {
                                if (properties.get(key) != null && !properties.get(key)
                                                                              .isEmpty()) {
                                    cleanProperties.put(key, properties.get(key));
                                }
                            });
        return cleanProperties;
    }


    @Override
    public Optional<BotConfig> find(String botConfigId) {
        return botConfigRepository.findById(botConfigId);
    }

    @Override
    public List<BotConfig> getAll(boolean archived) {
        return botConfigRepository.findAllByArchived(archived);
    }

    @Override
    public void delete(String botConfigId) {
        try {
            botExecutionService.deleteByBotConfigId(botConfigId);
            botConfigRepository.deleteById(botConfigId);
        } catch (RuntimeException e) {
            log.error("Failed to delete bot config with id {}", botConfigId);
        }
    }
}
