package app.bot.core.services;

import app.bot.core.domain.BotExecution;
import app.bot.core.repositories.BotExecutionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Alexander Voigt
 * <p>
 * Default implementation of {@link BotExecutionService}
 */
@Service
public class DefaultBotExecutionService implements BotExecutionService {

    private final static Logger log = LoggerFactory.getLogger(DefaultBotExecutionService.class);

    private final BotExecutionRepository botExecutionRepository;

    public DefaultBotExecutionService(BotExecutionRepository botExecutionRepository) {
        this.botExecutionRepository = botExecutionRepository;
    }

    @Override
    public Optional<BotExecution> findById(String botExecutionId) {
        try{
            return botExecutionRepository.findById(botExecutionId);
        }catch (IllegalArgumentException e){
            return Optional.empty();
        }
    }

    @Override
    public List<BotExecution> findByBotConfigId(String botConfigId) {
        return botExecutionRepository.findAllByBotConfigId(botConfigId);
    }

    @Override
    public Optional<BotExecution> create(BotExecution botExecution) {
        try {
            return Optional.of(botExecutionRepository.save(botExecution));
        } catch (RuntimeException e) {
            log.error("Failed to create bot execution: {}", e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public Optional<BotExecution> update(BotExecution botExecution) {
        try {
            if (botExecutionRepository.findById(botExecution.getId())
                                      .isEmpty()) {
                log.error(
                        "Failed to update bot execution: Could not find an existing bot execution" +
                                " " +
                                "with the given id.");
                return Optional.empty();
            }
            return Optional.of(botExecutionRepository.save(botExecution));
        } catch (RuntimeException e) {
            log.error("Failed to update bot execution: {}", e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public void deleteByBotConfigId(String botConfigId) {
        botExecutionRepository.findAllByBotConfigId(botConfigId)
                              .forEach(botExecutionRepository::delete);
    }

    @Override
    public int countBotExecutions(String botConfigId) {
        try{
            return botExecutionRepository.countByBotConfigId(botConfigId);
        }catch (RuntimeException e){
            return 0;
        }
    }
}
