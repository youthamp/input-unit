package app.bot.core.services;

import app.bot.core.domain.BotConfig;
import app.bot.core.rest.BotConfigRequestBody;

import java.util.List;
import java.util.Optional;

/**
 * @author Alexander Voigt
 * <p>
 * Service for creating and persisting valid {@link BotConfig}'s
 */
public interface BotConfigService {

    /**
     * Create a valid {@link BotConfig} from a {@link BotConfigRequestBody}.
     *
     * @param botConfigRequestBody See {@link BotConfigRequestBody}
     * @return {@link BotConfig} if it could be created and successfully persisted
     */
    BotConfig create(BotConfigRequestBody botConfigRequestBody);

    /**
     * Create or update {@link BotConfig}
     *
     * @param botConfig See {@link BotConfig}
     * @return {@link BotConfig} if it could be persisted or updated successfully
     */
    BotConfig saveOrUpdateBotConfig(BotConfig botConfig);

    /**
     * Get {@link BotConfig} by id
     *
     * @param botConfigId The bot config id
     * @return {@link BotConfig} if the id could be mapped to an existing entity
     */
    Optional<BotConfig> find(String botConfigId);

    /**
     * Get all {@link BotConfig}
     *
     * @param archived True if archived bot configurations should be returned, false otherwise
     * @return The list of {@link BotConfig}
     */
    List<BotConfig> getAll(boolean archived);

    /**
     * Delete {@link BotConfig} by id
     *
     * @param botConfigId The bot config id
     */
    void delete(String botConfigId);
}
