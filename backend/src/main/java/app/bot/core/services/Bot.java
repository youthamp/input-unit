package app.bot.core.services;

import org.quartz.InterruptableJob;

/**
 * @author Alexander Voigt
 * <p>
 * Central interface for bot implementations that can be executed by {@link BotManager}
 */
public interface Bot extends InterruptableJob {

    /**
     * @return A unique key used for identification
     */
    String getKey();

    String getGroup();
}
