package app.bot.core.services;

import app.bot.core.domain.BotConfig;
import app.bot.core.domain.BotSchedule;
import org.apache.commons.lang3.StringUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

@Component
public class BotDetailsProvider {

    private final static Logger log = LoggerFactory.getLogger(BotDetailsProvider.class);

    private final Set<Bot> bots;

    public BotDetailsProvider(Set<Bot> bots) {
        this.bots = bots;
    }

    public JobDetail createJobDetails(BotConfig botConfig) {
        JobBuilder jobBuilder = JobBuilder.newJob();
        setType(jobBuilder, botConfig);
        setJobData(jobBuilder, botConfig);
        jobBuilder.withIdentity(botConfig.getId(), botConfig.getBotGroup());
        return jobBuilder.build();
    }

    public Trigger createTrigger(BotConfig botConfig) {
        BotSchedule botSchedule = botConfig.getBotSchedule();
        TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger()
                                                               .withIdentity(botSchedule.getId());

        if (botSchedule.isManualExecution()) {
            return triggerBuilder.startNow()
                                 .build();
        }

        if (!StringUtils.isEmpty(botSchedule.getCron())) {
            triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule(botSchedule.getCron()));
        }
        return triggerBuilder.build();
    }

    private void setType(JobBuilder jobBuilder, BotConfig botConfig) {
        Bot bot = bots.stream()
                      .filter(b -> b.getKey()
                                    .equals(botConfig.getBotKey()))
                      .findFirst()
                      .orElseThrow(() -> new RuntimeException(
                              "Cold not find a corresponding bot implementation for bot key " +
                                      botConfig.getBotKey()));
        jobBuilder.ofType(bot.getClass());
    }

    private void setJobData(JobBuilder jobBuilder, BotConfig botConfig) {
        for (Map.Entry<String, String> property : botConfig.getProperties()
                                                           .entrySet()) {
            jobBuilder.usingJobData(property.getKey(), property.getValue());
        }
        jobBuilder.usingJobData(JobExecutionKeys.BOT_CONFIG_ID, botConfig.getId());
    }
}
