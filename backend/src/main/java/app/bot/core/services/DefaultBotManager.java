package app.bot.core.services;

import app.bot.core.domain.BotConfig;
import app.bot.core.rest.BotConfigRequestBody;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DefaultBotManager implements BotManager {

    private static final Logger logger = LoggerFactory.getLogger(DefaultBotManager.class);

    private final Scheduler scheduler;

    private final BotConfigService botConfigService;

    private final BotDetailsProvider botDetailsProvider;

    public DefaultBotManager(Scheduler scheduler, BotConfigService botConfigService,
                             BotDetailsProvider botDetailsProvider) {
        this.scheduler = scheduler;
        this.botConfigService = botConfigService;
        this.botDetailsProvider = botDetailsProvider;
    }

    @Override
    public BotConfig createBot(BotConfigRequestBody botConfigRequestBody) {
        BotConfig botConfig = botConfigService.create(botConfigRequestBody);
        try {
            if (!botConfig.getBotSchedule()
                          .isManualExecution() || botConfig.isActive()) {
                scheduleBot(botConfig);
            }
        } catch (SchedulerException e) {
            botConfigService.delete(botConfig.getId());
            throw new IllegalArgumentException("Failed to create bot: " + e.getMessage());
        }
        return botConfig;
    }

    @Override
    public boolean deleteBot(String botConfigId) {
        Optional<BotConfig> botConfigOptional = botConfigService.find(botConfigId);

        if (botConfigOptional.isPresent()) {
            BotConfig botConfig = botConfigOptional.get();

            try {
                scheduler.unscheduleJob(getTriggerKey(botConfig));
                botConfigService.delete(botConfigId);
                return true;
            } catch (SchedulerException e) {
                logger.error("Failed to unschedule bot with botConfigId {}: {}", botConfig.getId(),
                             e.getMessage());
            }
        }
        return false;
    }

    @Override
    public boolean activateBot(String botConfigId) {
        boolean activated = true;
        Optional<BotConfig> botConfigOptional = botConfigService.find(botConfigId);

        if (botConfigOptional.isPresent()) {
            BotConfig botConfig = botConfigOptional.get();
            try {
                botConfig.setActive(true);
                scheduleBot(botConfig);
                botConfigService.saveOrUpdateBotConfig(botConfig);
            } catch (SchedulerException e) {
                logger.error("Failed to activate bot: {}", e.getMessage());
                activated = false;
            }
        }
        return activated;
    }

    @Override
    public boolean deactivateBot(String botConfigId) {
        Optional<BotConfig> botConfigOptional = botConfigService.find(botConfigId);

        if (botConfigOptional.isPresent()) {
            BotConfig botConfig = botConfigOptional.get();
            try {
                botConfig.setActive(false);
                scheduler.interrupt(new JobKey(botConfigId));
                scheduler.pauseJob(new JobKey(botConfigId));
                scheduler.unscheduleJob(getTriggerKey(botConfig));
                botConfigService.saveOrUpdateBotConfig(botConfig);
                return true;
            } catch (SchedulerException e) {
                logger.error("Failed to deactivate bot: {}", e.getMessage());
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean executeBotManually(String botConfigId) {
        boolean jobStarted = false;
        Optional<BotConfig> botConfigOptional = botConfigService.find(botConfigId);
        try {
            if (botConfigOptional.isPresent()) {
                BotConfig botConfig = botConfigOptional.get();
                JobDetail jobDetail = botDetailsProvider.createJobDetails(botConfig);
                scheduler.addJob(jobDetail, true);
                scheduler.triggerJob(jobDetail.getKey());
                jobStarted = true;
            }
        } catch (SchedulerException e) {
            logger.error("Failed to execute bot manually: {}", e.getMessage());
        }
        return jobStarted;
    }

    @Override
    public boolean archiveBotConfig(String botConfigId) {
        boolean botDeactivated = deactivateBot(botConfigId);
        if (!botDeactivated) {
            return false;
        }
        Optional<BotConfig> botConfigOptional = botConfigService.find(botConfigId);
        try {
            if (botConfigOptional.isPresent()) {
                BotConfig botConfig = botConfigOptional.get();
                botConfig.setArchived(true);
                botConfigService.saveOrUpdateBotConfig(botConfig);
                return true;
            } else {
                return false;
            }
        } catch (RuntimeException e) {
            logger.error("Failed to archive bot: {}", e.getMessage());
            return false;
        }
    }

    @EventListener(ApplicationReadyEvent.class)
    public void scheduleBotConfigs() {
        botConfigService.getAll(false)
                        .stream()
                        .filter(BotConfig::isActive)
                        .forEach(botConfig -> {
                            try {
                                scheduleBot(botConfig);
                            } catch (SchedulerException e) {
                                logger.error("Failed to schedule bot on startup: {}",
                                             e.getMessage());
                            }
                        });
    }

    private void scheduleBot(BotConfig botConfig) throws SchedulerException {
        JobDetail jobDetail = botDetailsProvider.createJobDetails(botConfig);
        scheduler.scheduleJob(jobDetail, botDetailsProvider.createTrigger(botConfig));
    }

    private TriggerKey getTriggerKey(BotConfig botConfig) {
        return new TriggerKey(botConfig.getBotSchedule()
                                       .getId());
    }
}
