package app.bot.core.services;

/**
 * Class holding relevant keys which are used to store data inside {@link org.quartz.JobExecutionContext}
 */
public class JobExecutionKeys {
    public final static String BOT_CONFIG_ID = "bot_config_Id";
    public final static String BOT_EXECUTION_ID = "bot_execution_id";
}
