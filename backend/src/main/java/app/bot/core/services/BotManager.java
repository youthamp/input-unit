package app.bot.core.services;

import app.bot.core.domain.BotConfig;
import app.bot.core.rest.BotConfigRequestBody;

/**
 * @author Alexander Voigt
 * <p>
 * Service for managing bots defined by {@link BotConfig}'s
 */
public interface BotManager {

    /**
     * Create {@link BotConfig} from a request dto object
     *
     * @param botConfigRequestBody See {@link BotConfigRequestBody}
     * @return The created and persisted {@link BotConfig} if successful
     */
    BotConfig createBot(BotConfigRequestBody botConfigRequestBody);

    /**
     * Delete {@link BotConfig} by id
     *
     * @param botConfigId The {@link BotConfig} id
     * @return True if successful or false if not
     */
    boolean deleteBot(String botConfigId);

    /**
     * Activate {@link BotConfig} by id. Only activated bot configurations can be used to execute
     * bots
     *
     * @param botConfigId The {@link BotConfig} id
     * @return True if successful or false if not
     */
    boolean activateBot(String botConfigId);

    /**
     * Deactivate {@link BotConfig} by id. If deactivated, no more bots with this configuration
     * will be executed
     *
     * @param botConfigId The {@link BotConfig} id
     * @return True if successful or false if not
     */
    boolean deactivateBot(String botConfigId);

    /**
     * Execute bot defined by the {@link BotConfig} id immediately
     *
     * @param botConfigId The {@link BotConfig} id
     * @return True if successful or false if not
     */
    boolean executeBotManually(String botConfigId);

    /**
     * Archive {@link BotConfig} by id. Archived bot configurations can not be deleted and will
     * be set to inactive
     *
     * @param botConfigId The bot config id
     * @return True if it could be deleted successfully
     */
    boolean archiveBotConfig(String botConfigId);

}
