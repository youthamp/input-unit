package app.system.services;

import app.system.domain.AppUser;
import app.system.domain.Role;
import app.system.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class UserService {

    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }

    public void createUser(String username, String password, Role... roles) {
        AppUser userAccount = new AppUser();
        userAccount.setUsername(username);
        userAccount.setPassword(passwordEncoder.encode(password));
        userAccount.setRoles(Arrays.asList(roles));
        userRepository.save(userAccount);
    }

    public AppUser getUserByUsername(String username){
        return userRepository.findByUsername(username);
    }
}
