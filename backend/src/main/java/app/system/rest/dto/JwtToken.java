package app.system.rest.dto;

public class JwtToken {

    public JwtToken(String accessToken) {
        setAccessToken(accessToken);
    }

    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
