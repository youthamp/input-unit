package app.system.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiResponse {

    private Object data;
    private String message;
    private HttpStatus status;
    private int code;

    public ApiResponse(Object data, String message, HttpStatus status, int code) {
        this.status = status;
        this.message = message;
        this.data = data;
        this.setCode(code);
    }

    public HttpStatus getStatus() {
        return this.status;
    }

    public String getMessage() {
        return this.message;
    }

    public Object getData() {
        return this.data;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static Builder builder() {return new Builder();}

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public static class Builder {

        private Object data;

        private String message;

        private HttpStatus status;

        private int code;

        public Builder() {}

        public Builder withData(Object data){
            this.data = data;
            return this;
        }

        public Builder withMessage(String message){
            this.message = message;
            return this;
        }

        public Builder withStatus(HttpStatus status){
            this.status = status;
            return this;
        }

        public Builder withCode(int code){
            this.code = code;
            return this;
        }

        public Builder notFound(String message) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            this.status = status;
            this.code = status.value();
            this.message = message;
            return this;
        }

        public Builder ok(Object data) {
            HttpStatus status = HttpStatus.OK;
            this.status = status;
            this.code = status.value();
            this.data = data;
            return this;
        }

        public Builder error(String message) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            this.status = status;
            this.code = status.value();
            this.message = message;
            return this;
        }

        public Builder forbidden(String message) {
            HttpStatus status = HttpStatus.FORBIDDEN;
            this.status = status;
            this.code = status.value();
            this.message = message;
            return this;
        }

        public Builder unauthorized(String message) {
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            this.status = status;
            this.code = status.value();
            this.message = message;
            return this;
        }

        /**
         * @return Create {@link ApiResponse}
         */
        public ApiResponse build() {
            return new ApiResponse(data, message, status, code);
        }
    }
}
