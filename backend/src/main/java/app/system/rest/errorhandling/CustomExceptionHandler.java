package app.system.rest.errorhandling;

import app.system.rest.dto.ApiResponse;
import org.springframework.data.mapping.MappingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(MissingRequestParamsException.class)
    public final ResponseEntity<ApiResponse> handleMissingRequestParamsException(
            MissingRequestParamsException e, WebRequest webRequest) {
        return new ResponseEntity<>(new ApiResponse.Builder().error(
                String.format("Missing request params: %s", e.getMessage()))
                                                             .build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MappingException.class)
    public final ResponseEntity<ApiResponse> handleMappingException(MissingRequestParamsException e,
                                                                    WebRequest webRequest) {
        return new ResponseEntity<>(new ApiResponse.Builder().error(
                String.format("Failed to map data: %s", e.getMessage()))
                                                             .build(), HttpStatus.BAD_REQUEST);
    }
}