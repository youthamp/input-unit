package app.system.rest.errorhandling;

public class MissingRequestParamsException extends RuntimeException {
    public MissingRequestParamsException(String message){
        super(message);
    }
}
