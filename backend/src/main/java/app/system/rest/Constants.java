package app.system.rest;

public class Constants {
    public static class Web {
        public static final String BOT_MANAGER_URL = "/youthamp/input-unit/bot";
        public static final String TWITTER_DATA_URL = "/youthamp/input-unit/data/twitter";
        public static final String INSTAGRAM_DATA_URL = "/youthamp/input-unit/data/instagram";
        public static final String SENTIMENT_DATA_URL = "/youthamp/input-unit/data/sentiment";
        public static final String AUTHENTICATION_URL = "/youthamp/input-unit/authentication";
    }
}
