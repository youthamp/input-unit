package app.system.rest.controllers;

import app.system.security.jwt.JwtConfig;
import app.system.security.jwt.JwtTokenProvider;
import app.system.rest.dto.ApiResponse;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static app.system.rest.Constants.Web.AUTHENTICATION_URL;

@RestController
@RequestMapping(path = AUTHENTICATION_URL)
public class AuthenticationController {

    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public AuthenticationController(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @GetMapping(path = "/token")
    @ResponseBody
    public ApiResponse login() {
        try {
            return new ApiResponse.Builder().ok(
                    ImmutableMap.of(JwtConfig.ApiResponseDataFields.ACCESS_TOKEN,
                                    jwtTokenProvider.generateAccessToken()))
                                            .build();
        } catch (RuntimeException e) {
            return new ApiResponse.Builder().error(
                    String.format("Failed to generate access token: %s", e.getMessage()))
                                            .build();
        }

    }
}
