package app.system.security;

import app.system.domain.AppUser;
import app.system.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AppUserDetailsService implements UserDetailsService {

    private final static Logger log = LoggerFactory.getLogger(AppUserDetailsService.class);

    private final UserRepository userRepository;

    @Autowired
    public AppUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = userRepository.findByUsername(username);
        if (appUser == null) {
            log.debug("Could not find user '" + username + "' in database.");
            throw new UsernameNotFoundException(username);
        }

        return User.withUsername(username)
                   .password(appUser.getPassword())
                   .roles(getRoles(appUser))
                   .build();
    }

    private String[] getRoles(AppUser appUser){
        int numOfRoles = appUser.getRoles().size();

        String[] roles = new String[numOfRoles];
        for(int i = 0; i<numOfRoles; i++){
            roles[i] = appUser.getRoles().get(i).name();
        }
        return roles;
    }
}
