package app.system.security;

import app.system.security.jwt.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final static Logger log = LoggerFactory.getLogger(WebSecurityConfiguration.class);

    private final JwtTokenProvider jwtTokenProvider;
    private final AppUserDetailsService appUserDetailsService;
    private final JwtAccessTokenFilter accessTokenFilter;
    private final Environment environment;
    private final String INPUT_UNIT_FRONTEND_URL = "INPUT_UNIT_FRONTEND_URL";

    @Autowired
    public WebSecurityConfiguration(JwtTokenProvider jwtTokenProvider,
                                    AppUserDetailsService appUserDetailsService,
                                    JwtAccessTokenFilter accessTokenFilter,
                                    Environment environment) {

        this.jwtTokenProvider = jwtTokenProvider;
        this.appUserDetailsService = appUserDetailsService;
        this.accessTokenFilter = accessTokenFilter;
        this.environment = environment;
    }

    @Override
    public void configure(
            AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(appUserDetailsService)
                                    .passwordEncoder(passwordEncoder());
        authenticationManagerBuilder.authenticationProvider(getJwtAuthenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // authentication configurations
        http.authorizeRequests()
            .anyRequest()
            .authenticated()
            .and()
            .httpBasic()
            .and()
            .addFilterBefore(accessTokenFilter, BasicAuthenticationFilter.class);

        // cors and csrf configuration
        http.cors().configurationSource(corsConfigurationSource());

        http.sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.csrf()
            .disable();

        // exception handling
        http.exceptionHandling()
            .accessDeniedHandler(accessDeniedHandler())
            .authenticationEntryPoint(authenticationEntryPoint());
    }

    @Bean
    CustomAccessDeniedHandler accessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Bean
    CustomAuthenticationEntryPoint authenticationEntryPoint() {
        return new CustomAuthenticationEntryPoint();
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfig());
        return source;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private JwtAuthenticationProvider getJwtAuthenticationProvider() {
        return new JwtAuthenticationProvider(appUserDetailsService, jwtTokenProvider);
    }

    private CorsConfiguration corsConfig() {
        String frontendUrl = getFrontendUrl();
        String frontendUrlHttp = "http://" + frontendUrl;
        String frontendUrlHttps = "https://" + frontendUrl;
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowedOrigins(
                Arrays.asList(frontendUrlHttp, frontendUrlHttps));
        corsConfiguration.setAllowedMethods(
                Arrays.asList("OPTIONS", "POST", "PUT", "GET", "DELETE"));
        corsConfiguration.setAllowedHeaders(Collections.singletonList("*"));
        corsConfiguration.setAllowCredentials(true);
        return corsConfiguration;
    }

    private String getFrontendUrl() {
        String frontendUrl = null;
        try {

            frontendUrl = System.getenv(INPUT_UNIT_FRONTEND_URL);

        } catch (Exception e) {
            log.warn("Failed to get frontend url from system environment variable '" +
                             INPUT_UNIT_FRONTEND_URL + "': " + e.getMessage());
        }

        if (frontendUrl == null || frontendUrl.isEmpty()) {
            frontendUrl = environment.getProperty("frontend.url", "localhost:4200");
        }
        log.warn("CORS configuration will allow requests from the following url: '" + frontendUrl +
                         "'.");
        return frontendUrl;
    }
}
