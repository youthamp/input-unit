package app.system.security.jwt;

import io.jsonwebtoken.SignatureAlgorithm;

public class JwtConfig {

    public static SignatureAlgorithm SIGNATURE_ALG = SignatureAlgorithm.HS256;
    public static final String ISSUER = "youthamp";
    public static final String SHARED_SECRET_PROPERTY_KEY = "security.jwt.secret";
    // 30 min in ms
    public static final long ACCESS_TOKEN_EXPIRATION = 30 * 60 * 1000;

    public static class Claims {
        public static final String AUTHORITIES = "authorities";
    }

    public static class Header {
        public static final String ACCESS_TOKEN = "access_token";
    }

    public static class ApiResponseDataFields {
        public static final String ACCESS_TOKEN = "access_token";
    }
}
