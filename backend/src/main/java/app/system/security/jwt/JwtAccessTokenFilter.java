package app.system.security.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
public class JwtAccessTokenFilter extends OncePerRequestFilter {

    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    public void setJwtTokenProvider(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String header = request.getHeader("Authorization");

        if (header != null && header.startsWith("Bearer ")) {
            String accessToken = header.substring(7);

            Optional<String> renewedAccessTokenOpt = jwtTokenProvider.renewExpirationDateIfValid(accessToken);
            if(renewedAccessTokenOpt.isPresent()){
                accessToken = renewedAccessTokenOpt.get();
                response.setHeader(JwtConfig.Header.ACCESS_TOKEN, accessToken);
            }
            SecurityContextHolder.getContext()
                                 .setAuthentication(new JwtAuthentication(accessToken));
        }

        filterChain.doFilter( request, response );
    }
}
