package app.system.security.jwt;

import app.system.security.AppUserDetailsService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class JwtAuthenticationProvider implements AuthenticationProvider {

    private final AppUserDetailsService appUserDetailsService;
    private final JwtTokenProvider jwtTokenProvider;

    public JwtAuthenticationProvider(AppUserDetailsService appUserDetailsService,
                                     JwtTokenProvider jwtTokenProvider) {
        this.appUserDetailsService = appUserDetailsService;
        this.jwtTokenProvider = jwtTokenProvider;
    }


    @Override
    public Authentication authenticate(
            Authentication authentication) throws AuthenticationException {
        String jwtToken = (String) authentication.getCredentials();
        JwtAuthentication jwtAuthentication = jwtTokenProvider.getAuthentication(jwtToken);
        appUserDetailsService.loadUserByUsername(jwtAuthentication.getName());
        return jwtAuthentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthentication.class.equals(authentication);
    }
}
