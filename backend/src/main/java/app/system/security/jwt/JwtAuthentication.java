package app.system.security.jwt;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JwtAuthentication implements Authentication {

    private String username;
    private boolean isAuthenticated;
    private String token;
    private Collection<? extends GrantedAuthority> authorities;

    public JwtAuthentication(String token) {
        this.token = token;
        this.setAuthenticated(false);
    }

    public JwtAuthentication(String username, Collection<? extends GrantedAuthority> authorities ){
        this.username = username;
        this.authorities = authorities;
        this.setAuthenticated( true );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        this.isAuthenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return username;
    }
}
