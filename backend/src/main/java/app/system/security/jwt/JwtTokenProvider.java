package app.system.security.jwt;

import app.system.domain.AppUser;
import app.system.services.UserService;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.xml.bind.DatatypeConverter;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

@Component
public class JwtTokenProvider {

    private UserService userService;
    private Environment environment;

    @Autowired
    public void setDependencies(UserService userService, Environment environment) {
        this.userService = userService;
        this.environment = environment;
    }

    public String generateAccessToken() {
        String username = SecurityContextHolder.getContext()
                                               .getAuthentication()
                                               .getName();
        return generateAccessToken(username);
    }

    public String generateAccessToken(String username) {
        AppUser appUser = userService.getUserByUsername(username);
        if (appUser == null) {
            throw new UsernameNotFoundException("User '" + username + "' not found.");
        }

        Date currentDate = new Date();
        Date expirationDate = new Date(new Date().getTime() + JwtConfig.ACCESS_TOKEN_EXPIRATION);

        return Jwts.builder()
                   .setIssuer(JwtConfig.ISSUER)
                   .setSubject(username)
                   .setIssuedAt(currentDate)
                   .setExpiration(expirationDate)
                   .claim(JwtConfig.Claims.AUTHORITIES, appUser.getRoles())
                   .signWith(JwtConfig.SIGNATURE_ALG, getSharedSecret())
                   .compact();
    }

    public JwtAuthentication getAuthentication(String jwtToken) {
        JwtAuthentication jwtAuthentication;
        try {
            Claims claims = Jwts.parser()
                                .setSigningKey(
                                        DatatypeConverter.parseBase64Binary(getSharedSecret()))
                                .parseClaimsJws(jwtToken)
                                .getBody();

            String subject = claims.getSubject();
            Collection<GrantedAuthority> authorities =
                    (Collection<GrantedAuthority>) claims.get(JwtConfig.Claims.AUTHORITIES);
            jwtAuthentication = new JwtAuthentication(subject, authorities);

        } catch (Exception e) {
            throw new InvalidJwtException(e.getMessage());
        }

        return jwtAuthentication;
    }

    public boolean isValidAndNotExpired(String jwtToken) {
        boolean isValidAndNotExpired = true;

        try {
            Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(getSharedSecret()))
                .parseClaimsJws(jwtToken)
                .getBody();

        } catch (Exception e) {
            isValidAndNotExpired = false;
        }

        return isValidAndNotExpired;
    }

    public Optional<String> renewExpirationDateIfValid(String jwtToken) {
        if (isValidAndNotExpired(jwtToken)) {
            String username = getUsername(jwtToken);
            return Optional.of(generateAccessToken(username));
        }
        return Optional.empty();
    }

    private String getUsername(String jwtToken) {
        try {
            return Jwts.parser()
                       .setSigningKey(DatatypeConverter.parseBase64Binary(getSharedSecret()))
                       .parseClaimsJws(jwtToken)
                       .getBody()
                       .getSubject();

        } catch (Exception e) {
            return null;
        }
    }

    private String getSharedSecret() {
        String jwtSharedSecret = environment.getProperty(JwtConfig.SHARED_SECRET_PROPERTY_KEY, "");

        if (jwtSharedSecret.isEmpty()) {
            throw new RuntimeException(
                    "Property '" + JwtConfig.SHARED_SECRET_PROPERTY_KEY + "' is not configured.");
        }
        return jwtSharedSecret;
    }
}
