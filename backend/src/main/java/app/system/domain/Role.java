package app.system.domain;

public enum Role {
    USER,
    ADMIN
}
