package app.system.init;

import app.bot.platforms.instagram.domain.InstagramUser;
import app.bot.platforms.instagram.domain.Post;
import app.bot.platforms.instagram.repository.InstagramUserRepository;
import app.bot.platforms.instagram.service.InstagramService;
import app.system.domain.Role;
import app.system.security.AppUserDetailsService;
import app.system.services.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BootstrapData {

    private final static Logger log = LoggerFactory.getLogger(BootstrapData.class);

    private final String DOCKER_ENV_INPUT_UNIT_ADMIN_USERNAME = "INPUT_UNIT_ADMIN_USERNAME";
    private final String DOCKER_ENV_INPUT_UNIT_ADMIN_PASSWORD = "INPUT_UNIT_ADMIN_PASSWORD";
    private final String PROPERTY_INPUT_UNIT_ADMIN_USERNAME = "admin.username";
    private final String PROPERTY_INPUT_UNIT_ADMIN_PASSWORD = "admin.password";
    private final UserService userService;
    private final Environment environment;
    private final InstagramService instagramService;
    private final InstagramUserRepository instagramUserRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void createAdminUser() {
        var adminUsername = getAdminUsername();
        var adminPassword = getAdminPassword();

        if (adminUsername.isEmpty() || adminPassword.isEmpty()) {
            throw new RuntimeException("Credential properties for admin user not set.");
        }
        userService.createUser(adminUsername, adminPassword, Role.ADMIN);
    }

    private String getAdminUsername() {
        String adminUsername = environment.getProperty(DOCKER_ENV_INPUT_UNIT_ADMIN_USERNAME, "");
        if (adminUsername.isBlank()) {
            log.warn("Docker environment variable for admin username not set.");
            return environment.getProperty(PROPERTY_INPUT_UNIT_ADMIN_USERNAME, "");
        }
        return adminUsername;
    }

    private String getAdminPassword() {
        String adminUsername = environment.getProperty(DOCKER_ENV_INPUT_UNIT_ADMIN_PASSWORD, "");
        if (adminUsername.isBlank()) {
            log.warn("Docker environment variable for admin password not set.");
            return environment.getProperty(PROPERTY_INPUT_UNIT_ADMIN_PASSWORD, "");
        }
        return adminUsername;
    }
}
