import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {DefaultModule} from './layouts/default/default.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {XhrInterceptor} from './modules/login/auth';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthService} from './modules/login/auth.service';
import {AuthGuardService} from './modules/login/auth-guard.service';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatRadioModule} from '@angular/material/radio';
import {CronEditorModule} from 'ngx-cron-editor';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DefaultModule,
    MatFormFieldModule,
    MatDialogModule,
    MatTabsModule,
    MatRadioModule,
    CronEditorModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AuthService, AuthGuardService, { provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
