import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DefaultComponent} from './layouts/default/default.component';
import {BotComponent} from './modules/bot/bot.component';
import {TweetsComponent} from './modules/data/twitter/tweets/tweets.component';
import {TwitterBotStatisticsComponent} from './modules/data/twitter/twitter-bot-statistics/twitter-bot-statistics.component';
import {LoginComponent} from './modules/login/login.component';
import {AuthGuardService as AuthGuard} from './modules/login/auth-guard.service';

const routes: Routes = [{
  path: '',
  component: DefaultComponent,
  children: [
    {
      path: '',
      canActivate: [AuthGuard],
      component: BotComponent
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'bots',
      canActivate: [AuthGuard],
      component: BotComponent
    },
    {
      path: 'twitter-data/:botExecutionId',
      canActivate: [AuthGuard],
      component: TweetsComponent
    },
    {
      path: 'twitter-bot-statistics/:botConfigId',
      canActivate: [AuthGuard],
      component: TwitterBotStatisticsComponent
    }]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
