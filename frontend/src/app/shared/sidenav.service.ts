import {Injectable} from '@angular/core';
import {MatSidenav} from "@angular/material/sidenav";
import {MatDrawerToggleResult} from "@angular/material/sidenav/drawer";

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  private sidenav!: MatSidenav;

  constructor() {
  }

  public toggle(): Promise<MatDrawerToggleResult> {
    return this.sidenav.toggle();
  }

  setSidenav(sidenav: MatSidenav) {
    this.sidenav = sidenav;
  }
}
