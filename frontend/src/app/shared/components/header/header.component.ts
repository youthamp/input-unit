import {Component, OnInit} from '@angular/core';
import {SidenavService} from "../../sidenav.service";
import {AuthService} from "../../../modules/login/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private sidenavService: SidenavService, private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  toggle(): void {
    this.sidenavService.toggle();
  }

  logout(){
    this.authService.logout();
  }
}
