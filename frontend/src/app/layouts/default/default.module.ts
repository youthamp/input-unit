import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DefaultComponent} from './default.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from 'src/app/shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {SidenavService} from '../../shared/sidenav.service';
import {BotComponent} from '../../modules/bot/bot.component';
import {MatTableModule} from '@angular/material/table';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatMenuModule} from '@angular/material/menu';
import {MatOptionModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {TweetsComponent} from '../../modules/data/twitter/tweets/tweets.component';
import {TwitterBotStatisticsComponent} from '../../modules/data/twitter/twitter-bot-statistics/twitter-bot-statistics.component';
import {ChartsModule} from 'ng2-charts';
import {LoginComponent} from '../../modules/login/login.component';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorModule} from '@angular/material/paginator';
import {TwitterBotSetupDialogComponent} from '../../modules/bot/dialogs/twitter-bot-setup/twitter-bot-setup-dialog.component';
import {CronEditorModule} from 'ngx-cron-editor';
import {MatTabsModule} from '@angular/material/tabs';
import {ToastrModule} from 'ngx-toastr';
// tslint:disable-next-line:max-line-length
import {InstagramBotSetupDialogComponent} from '../../modules/bot/dialogs/instagram-bot-setup/instagram-bot-setup-dialog.component';
import {BotDetailsComponent} from '../../modules/bot/dialogs/bot-details/bot-details.component';
import {
  GoogleSentimentBotSetupDialogComponent
} from '../../modules/bot/dialogs/google-sentiment-setup/google-sentiment-bot-setup-dialog.component';

@NgModule({
  declarations: [
    DefaultComponent,
    BotComponent,
    TwitterBotStatisticsComponent,
    TweetsComponent,
    TwitterBotSetupDialogComponent,
    InstagramBotSetupDialogComponent,
    GoogleSentimentBotSetupDialogComponent,
    BotDetailsComponent,
    LoginComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatTableModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    MatMenuModule,
    MatOptionModule,
    MatSelectModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    FormsModule,
    ChartsModule,
    MatCardModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    CronEditorModule,
    MatTabsModule,
    ToastrModule.forRoot()
  ],
  exports: [],
  entryComponents: [TwitterBotSetupDialogComponent],
  providers: [
    SidenavService,
    {
      provide: MatDialogRef,
      useValue: {}
    },
  ]
})
export class DefaultModule {

  constructor() {
  }
}
