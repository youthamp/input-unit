import {Component, OnInit} from '@angular/core';
import {ChartDataSets, ChartType} from 'chart.js';
import {Color} from 'ng2-charts';
import {TwitterService} from '../twitter.service';
import {ActivatedRoute, Router} from '@angular/router';
import * as _ from 'lodash';
import {TwitterBotStatistic} from './twitter-bot-statistics';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-twitter-bot-statistics',
  templateUrl: './twitter-bot-statistics.component.html',
  styleUrls: ['./twitter-bot-statistics.component.scss']
})
export class TwitterBotStatisticsComponent implements OnInit {

  constructor(private twitterService: TwitterService, private activatedRoute: ActivatedRoute, private router: Router) {
  }

  botStatisticsObs!: Observable<TwitterBotStatistic[]>;
  botStatistics!: TwitterBotStatistic[];
  lineChartLabels!: string[];
  datesArray!: any[];
  botConfigId!: number;
  lineChartData!: ChartDataSets[];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgb(255, 64, 129)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType: ChartType = 'bar';

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.botConfigId = _.get(params, 'botConfigId', '');
      this.botStatisticsObs = this.twitterService.getBotStatistics(this.botConfigId);


      this.botStatisticsObs.subscribe(botStatistics => {
        this.lineChartData = [
          {data: botStatistics.map(botStatistic => botStatistic.collectedTweets), label: 'collected tweets'}
        ];
        this.datesArray = botStatistics.map(botStatistic => botStatistic.collectedOn);
        this.botStatistics = botStatistics;
        this.lineChartLabels = botStatistics.map(botStatistic => this.getFormattedDate(botStatistic.collectedOn));
      });
    });
  }

  getFormattedDate(date: Date): string {
    const castedDate = new Date(date);
    return castedDate.getHours() + ':' + castedDate.getMinutes() + ':' + castedDate.getSeconds();
    // return castedDate.getDay() + "." + castedDate.getMonth() + "." + castedDate.getFullYear()
    //   + " " + castedDate.getHours() + ":" + castedDate.getMinutes();
  }

  chartClicked(event: any): void {
    const dateIndex: number = event.active[0]._index;
    try {
      this.router.navigate(['twitter-data/:botExecutionId', {
        botExecutionId: this.botStatistics[dateIndex].botExecutionId
      }]);
    } catch (e) {
      console.warn('Failed to extract data from statistics which is necessary to view corresponding tweet data.');
    }
  }

  downloadTweetsAsCsv(): void {
    console.log('Downloading tweets as csv...');
  }
}
