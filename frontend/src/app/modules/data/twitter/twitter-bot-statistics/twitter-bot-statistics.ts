export interface TwitterBotStatistic {
  botExecutionId: string;
  collectedOn: Date;
  tweetIds: string[];
  collectedTweets: number;
}
