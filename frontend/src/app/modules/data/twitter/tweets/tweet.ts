export interface Tweet {
  message: string;
  retweetCount: number;
  favoriteCount: number;
  retweetFavoriteCount: number;
  createdAt: Date;
}
