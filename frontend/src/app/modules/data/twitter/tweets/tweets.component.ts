import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as _ from 'lodash';
import {TwitterService} from '../twitter.service';
import {Tweet} from './tweet';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'tweets-table',
  templateUrl: './tweets.component.html',
  styleUrls: ['./tweets.component.scss']
})
export class TweetsComponent implements OnInit {

  dataSource!: MatTableDataSource<Tweet>;
  displayedColumns = ['createdAt', 'retweetCount', 'favoriteCount', 'retweetFavoriteCount', 'message'];
  private collectedOn: any;
  private botExecutionId!: string;

  pageSize!: number;
  pageIndex!: number;
  length!: number;


  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private activatedRoute: ActivatedRoute, private twitterService: TwitterService) {
    this.pageIndex = 0;
    this.pageSize = 5;
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.collectedOn = _.get(params, 'collectedOn', '');
      this.botExecutionId = _.get(params, 'botExecutionId', '');
      this.twitterService.getTweets(this.botExecutionId, this.pageIndex, this.pageSize).subscribe(responsePage => {
        this.dataSource = responsePage.content;
        this.length = responsePage.totalElements;
      });
    });
  }

  onPaginate(event: PageEvent): void {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.twitterService.getTweets(this.botExecutionId, event.pageIndex, event.pageSize).subscribe(responsePage => {
      this.dataSource = responsePage.content;
    });
  }
}
