import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {TwitterBotStatistic} from './twitter-bot-statistics/twitter-bot-statistics';
import {Page} from '../pagination';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TwitterService {

  private readonly url: string = environment.backendUrl + '/youthamp/input-unit/data/twitter';

  constructor(@Inject(HttpClient) private httpClient: HttpClient) {

  }

  getBotStatistics(botConfigId: number): Observable<TwitterBotStatistic[]> {
    const httpParams = new HttpParams().set('botConfigId', botConfigId.toString());
    const options = {params: httpParams};
    return this.httpClient.get<TwitterBotStatistic[]>(this.url + '/twitter-bot-statistics', options);
  }

  getTweets(botExecutionId: string, pageIndex: number, pageSize: number): Observable<Page> {
    const httpParams = new HttpParams()
      .set('botExecutionId', botExecutionId)
      .set('page', String(pageIndex))
      .set('pageSize', String(pageSize));
    const options = {params: httpParams};
    const url = this.url + '/tweets/paging?';
    return this.httpClient.get<any>(url, options);
  }

  downloadTweetsAsCsv(botConfigId: string): void {
    const filename = 'tweets_botConfigId_' + botConfigId + '.csv';
    const url = this.url + '/tweets/export?botConfigId=' + botConfigId;
    this.httpClient.get(url, {responseType: 'text'}).subscribe(csvData => {
      const blob = new Blob(['\ufeff' + csvData], {
        type: 'text/csv;charset=utf-8;'
      });
      const dwldLink = document.createElement('a');
      const objectURL = URL.createObjectURL(blob);
      dwldLink.setAttribute('href', objectURL);
      dwldLink.setAttribute('download',  filename);
      dwldLink.style.visibility = 'hidden';
      document.body.appendChild(dwldLink);
      dwldLink.click();
      document.body.removeChild(dwldLink);
    });
  }
}
