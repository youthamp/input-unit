import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from './auth.service';
import {Credentials} from './auth';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  credentials: Credentials = {username: '', password: ''};
  form!: FormGroup;
  public loginInvalid!: boolean;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.form = this.fb.group({
      username: [''],
      password: ['', Validators.required]
    });
  }

  async login(): Promise<void> {
    if (this.form.valid) {
      this.authService.login(this.credentials).subscribe(result => {
        localStorage.setItem('access_token', result.data.access_token);
        this.router.navigate(['/bots']);

      }, () => {
        this.loginInvalid = true;
      });
    }
  }
}
