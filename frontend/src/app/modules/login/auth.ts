import {Injectable} from "@angular/core";
import {HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {AuthService} from "./auth.service";

export interface Credentials {
  username: string;
  password: string;
}

export interface JwtDecoded {
  exp: number;
}

@Injectable()
export class XhrInterceptor implements HttpInterceptor {

  constructor(public authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler) {

    let access_token: string | null = localStorage.getItem("access_token");

    if(this.authService.isAuthenticated()){
      request = request.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + access_token)
      });
    } else {
      localStorage.removeItem("access_token");
    }

    return next.handle(request);
  }
}

