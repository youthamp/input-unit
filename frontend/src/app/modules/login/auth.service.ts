import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Credentials, JwtDecoded} from './auth';
import {Observable} from 'rxjs';
import jwtDecode from 'jwt-decode';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  login(credentials: Credentials): Observable<any> {
    const headers = new HttpHeaders({
      Authorization: 'Basic ' + btoa(credentials.username + ':' + credentials.password)
    });

    const httpOptions = {
      headers
    };
    const url = environment.backendUrl + '/youthamp/input-unit/authentication/token';
    return this.httpClient.get<any>(url, httpOptions);
  }

  logout(): void {
    localStorage.removeItem('access_token');
    this.router.navigate(['/login']);
  }

  public isAuthenticated(): boolean {
    try {
      const accessToken: string = localStorage.getItem('access_token') || '';
      const jwtDecoded: JwtDecoded = jwtDecode(accessToken);
      return !(accessToken && Date.now() > jwtDecoded.exp * 1000);

    } catch (e) {
      return false;
    }
  }
}
