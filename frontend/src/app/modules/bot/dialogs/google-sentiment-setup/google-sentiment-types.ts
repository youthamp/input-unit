export interface GoogleSentimentSetupDialogConfig {
  botKey: string;
  dataTypes: string;
  cron: string;
  id?: string;
  executionType?: string;
  editMode?: boolean;
}
