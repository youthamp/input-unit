import {Component, Inject, OnInit} from '@angular/core';
import {CronOptions} from 'ngx-cron-editor/src/CronOptions';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {GoogleSentimentSetupDialogConfig} from './google-sentiment-types';

@Component({
  selector: 'app-google-sentiment-setup',
  templateUrl: './google-sentiment-bot-setup-dialog.component.html',
  styleUrls: ['../setup-dialog.scss', './google-sentiment-bot-setup-dialog.component.scss']
})
export class GoogleSentimentBotSetupDialogComponent implements OnInit {

  cronOptions!: CronOptions;
  cronFormControl!: FormControl;
  cronFormGroup!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<GoogleSentimentBotSetupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public googleSentimentBotConfig: GoogleSentimentSetupDialogConfig, builder: FormBuilder) {
    this.cronFormControl = new FormControl(this.googleSentimentBotConfig.cron);
    this.cronFormGroup = builder.group({
    });
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): void {
    this.googleSentimentBotConfig.cron = this.cronFormControl.value;
  }

  ngOnInit(): void {
    this.cronOptions  = {
      formInputClass: 'form-control cron-editor-input',
      formSelectClass: 'form-control cron-editor-select',
      formRadioClass: 'cron-editor-radio',
      formCheckboxClass: 'cron-editor-checkbox',
      defaultTime: '00:00:00',
      hideMinutesTab: false,
      hideHourlyTab: false,
      hideDailyTab: false,
      hideWeeklyTab: false,
      hideMonthlyTab: true,
      hideYearlyTab: true,
      hideAdvancedTab: false,
      hideSpecificWeekDayTab: false,
      hideSpecificMonthWeekTab: true,
      use24HourTime: true,
      hideSeconds: false,
      cronFlavor: 'quartz'
    };
  }
}
