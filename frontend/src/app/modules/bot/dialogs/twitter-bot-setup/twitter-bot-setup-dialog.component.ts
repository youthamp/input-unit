import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ExecutionType, SearchType} from '../../bot-details';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {CronOptions} from 'ngx-cron-editor/src/CronOptions';
import {TwitterSetupDialogConfig} from './twitter-bot-types';

@Component({
  selector: 'app-twitter-setup-dialog',
  templateUrl: 'twitter-bot-setup-dialog.html',
  styleUrls: ['../setup-dialog.scss', '/twitter-bot-setup-dialog.scss']
})
export class TwitterBotSetupDialogComponent implements OnInit {

  searchTypes: string[] = [SearchType.PERSONAL_TIMELINE, SearchType.USER_TIMELINE, SearchType.HASHTAGS];
  cronOptions!: CronOptions;
  cronFormControl!: FormControl;
  cronFormGroup!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<TwitterBotSetupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public twitterBotConfig: TwitterSetupDialogConfig, builder: FormBuilder) {
    this.cronFormControl = new FormControl(this.twitterBotConfig.cron);
    this.cronFormGroup = builder.group({
    });
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): void {
    this.twitterBotConfig.cron = this.cronFormControl.value;
  }

  ngOnInit(): void {
    this.cronOptions  = {
      formInputClass: 'form-control cron-editor-input',
      formSelectClass: 'form-control cron-editor-select',
      formRadioClass: 'cron-editor-radio',
      formCheckboxClass: 'cron-editor-checkbox',
      defaultTime: '00:00:00',
      hideMinutesTab: false,
      hideHourlyTab: false,
      hideDailyTab: false,
      hideWeeklyTab: false,
      hideMonthlyTab: true,
      hideYearlyTab: true,
      hideAdvancedTab: false,
      hideSpecificWeekDayTab: false,
      hideSpecificMonthWeekTab: true,
      use24HourTime: true,
      hideSeconds: false,
      cronFlavor: 'quartz'
    };
  }
}
