export interface TwitterSetupDialogConfig {
  botKey: string;
  id?: string;
  hashtags?: string;
  cron: string;
  screenName?: string;
  searchType?: string;
  executionType?: string;
  editMode?: boolean;
  maxCount?: number;
}

export class TwitterDefaultProperties {
  static DEFAULT_MAX_COUNT = 600;
}
