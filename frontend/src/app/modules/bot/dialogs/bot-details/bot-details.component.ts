import {Component, Inject, OnInit} from '@angular/core';
import {BotDetails} from '../../bot-details';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-instagram-setup-dialog',
  templateUrl: './bot-details.component.html',
  styleUrls: ['../setup-dialog.scss', './bot-details.component.scss']
})
export class BotDetailsComponent implements OnInit {


  constructor(
    public dialogRef: MatDialogRef<BotDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public botDetails: BotDetails) {
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

  getReadableProperties(botDetails: BotDetails): string{
    return JSON.stringify(botDetails.properties);
  }
}
