export interface InstagramSetupDialogConfig {
  botKey: string;
  id?: string;
  cron: string;
  executionType?: string;
  editMode?: boolean;
}
