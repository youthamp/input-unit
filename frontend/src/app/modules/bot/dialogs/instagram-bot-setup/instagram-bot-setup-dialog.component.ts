import {Component, Inject, OnInit} from '@angular/core';
import {CronOptions} from 'ngx-cron-editor/src/CronOptions';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {InstagramSetupDialogConfig} from './instagram-bot-types';

@Component({
  selector: 'app-instagram-setup-dialog',
  templateUrl: './instagram-bot-setup-dialog.component.html',
  styleUrls: ['../setup-dialog.scss', './instagram-bot-setup-dialog.component.scss']
})
export class InstagramBotSetupDialogComponent implements OnInit {

  cronOptions!: CronOptions;
  cronFormControl!: FormControl;
  cronFormGroup!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<InstagramBotSetupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public instagramBotConfig: InstagramSetupDialogConfig, builder: FormBuilder) {
    this.cronFormControl = new FormControl(this.instagramBotConfig.cron);
    this.cronFormGroup = builder.group({
    });
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): void {
    this.instagramBotConfig.cron = this.cronFormControl.value;
  }

  ngOnInit(): void {
    this.cronOptions  = {
      formInputClass: 'form-control cron-editor-input',
      formSelectClass: 'form-control cron-editor-select',
      formRadioClass: 'cron-editor-radio',
      formCheckboxClass: 'cron-editor-checkbox',
      defaultTime: '00:00:00',
      hideMinutesTab: false,
      hideHourlyTab: false,
      hideDailyTab: false,
      hideWeeklyTab: false,
      hideMonthlyTab: true,
      hideYearlyTab: true,
      hideAdvancedTab: false,
      hideSpecificWeekDayTab: false,
      hideSpecificMonthWeekTab: true,
      use24HourTime: true,
      hideSeconds: false,
      cronFlavor: 'quartz'
    };
  }
}
