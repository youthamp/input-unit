import {Injectable} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {TwitterBotSetupDialogComponent} from './twitter-bot-setup/twitter-bot-setup-dialog.component';
import {BotDetails, BotKey, ExecutionType, SearchType} from '../bot-details';
import {TwitterDefaultProperties, TwitterSetupDialogConfig} from './twitter-bot-setup/twitter-bot-types';
import {InstagramBotSetupDialogComponent} from './instagram-bot-setup/instagram-bot-setup-dialog.component';
import {BotDetailsComponent} from './bot-details/bot-details.component';
import {GoogleSentimentBotSetupDialogComponent} from './google-sentiment-setup/google-sentiment-bot-setup-dialog.component';
import {GoogleSentimentSetupDialogConfig} from './google-sentiment-setup/google-sentiment-types';

@Injectable({
  providedIn: 'root'
})
export class SetupDialogService {

  DEFAULT_WIDTH_SETUP_DIALOG = '40%';
  SMALL_WIDTH_SETUP_DIALOG = '20%';
  DEFAULT_CRON = '0/3 * * * * ?';

  constructor(public dialog: MatDialog) {
  }

  openBotDetailsDialog(botDetails: any): MatDialogRef<BotDetailsComponent> {
    return this.dialog.open(BotDetailsComponent, {
      width: this.SMALL_WIDTH_SETUP_DIALOG,
      data: botDetails
    });
  }

  openTwitterSetupDialog(): MatDialogRef<TwitterBotSetupDialogComponent> {
    return this.dialog.open(TwitterBotSetupDialogComponent, {
      width: this.DEFAULT_WIDTH_SETUP_DIALOG,
      data: this.getDefaultTwitterSetupDialogConfig()
    });
  }

  openInstagramSetupDialog(): MatDialogRef<InstagramBotSetupDialogComponent> {
    return this.dialog.open(InstagramBotSetupDialogComponent, {
      width: this.DEFAULT_WIDTH_SETUP_DIALOG,
      data: this.getDefaultInstagramSetupDialogConfig()
    });
  }

  openGoogleSentimentSetupDialog(): MatDialogRef<GoogleSentimentBotSetupDialogComponent> {
    return this.dialog.open(GoogleSentimentBotSetupDialogComponent, {
      width: this.DEFAULT_WIDTH_SETUP_DIALOG,
      data: this.getDefaultGoogleSentimentSetupDialogConfig()
    });
  }

  private getDefaultTwitterSetupDialogConfig(): TwitterSetupDialogConfig {
    return {
      botKey: BotKey.TWITTER,
      searchType: SearchType.PERSONAL_TIMELINE,
      cron: this.DEFAULT_CRON,
      executionType: ExecutionType.SCHEDULE,
      maxCount: TwitterDefaultProperties.DEFAULT_MAX_COUNT
    };
  }

  private getDefaultInstagramSetupDialogConfig(): TwitterSetupDialogConfig {
    return {
      botKey: BotKey.INSTAGRAM,
      cron: this.DEFAULT_CRON,
      executionType: ExecutionType.SCHEDULE
    };
  }

  private getDefaultGoogleSentimentSetupDialogConfig(): GoogleSentimentSetupDialogConfig {
    return {
      botKey: BotKey.GOOGLE_SENTIMENT,
      cron: this.DEFAULT_CRON,
      executionType: ExecutionType.SCHEDULE,
      dataTypes: 'TWEET,INSTAGRAM_POST'
    };
  }
}
