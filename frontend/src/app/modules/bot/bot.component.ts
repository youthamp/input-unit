import {Component, OnInit} from '@angular/core';
import {BotDetails, BotKey} from './bot-details';
import {BotService} from './bot.service';
import {MatDialog} from '@angular/material/dialog';
import {TwitterService} from '../data/twitter/twitter.service';
import {ToastrService} from 'ngx-toastr';
import {SetupDialogService} from './dialogs/setup-dialog.service';
import {ContinueDialogComponent} from '../../shared/components/continue-dialog/continue-dialog.component';

@Component({
  selector: 'app-bot',
  templateUrl: './bot.component.html',
  styleUrls: ['./bot.component.scss']
})
export class BotComponent implements OnInit {

  displayedColumns: string[] = ['id', 'active', 'executionDetails', 'botKey', 'schedule', 'actions'];
  botDetails!: BotDetails[];

  constructor(private botConfigService: BotService, private twitterService: TwitterService,
              public dialog: MatDialog, private toastService: ToastrService, private setupDialogService: SetupDialogService) {
  }

  ngOnInit(): void {
    this.updateBotConfigView();
  }

  deleteBotConfig(id: number): void {
    const dialogRef = this.dialog.open(ContinueDialogComponent, {
      data: {
        warningMessage: 'If there is any data collected by the corresponding bot, then all references to this ' +
          'configuration will be removed too! The data itself will be kept in the database.'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.botConfigService.deleteBotConfig(id.toString()).subscribe(apiResponse => {
          if (apiResponse.code === 200) {
            this.showSuccessToast('Success', 'Bot configuration deleted');
            this.updateBotConfigView();
          } else {
            this.showErrorToast('Error', apiResponse.message);
          }
        });
      }
    });
  }

  archiveBotConfig(id: number): void {
    this.botConfigService.archiveBotConfig(id.toString()).subscribe(apiResponse => {
      if (apiResponse.code === 200) {
        this.showSuccessToast('Success', 'Bot configuration archived');
        this.updateBotConfigView();
      } else {
        this.showErrorToast('Error', apiResponse.message);
      }
    });
  }

  downloadData(botConfig: BotDetails): void {
    const botConfigId = botConfig.id;
    if (botConfig.botKey === BotKey.TWITTER) {
      this.twitterService.downloadTweetsAsCsv(botConfigId);
    }
  }

  openSetupDialog(botKey: string): void {
    if (botKey === BotKey.TWITTER) {
      this.setupDialogService.openTwitterSetupDialog().afterClosed().subscribe(twitterBotConfig => {
        if (twitterBotConfig !== undefined) {
          this.botConfigService.createTwitterBot(twitterBotConfig).subscribe(apiResponse => {
            if (apiResponse.code === 200) {
              this.showSuccessToast('Success', 'Bot with key ' + botKey + ' created');
              this.updateBotConfigView();
            } else {
              this.showErrorToast('Error', apiResponse.message);
            }
          });
        }
      });
    } else if (botKey === BotKey.INSTAGRAM) {
      this.setupDialogService.openInstagramSetupDialog().afterClosed().subscribe(instagramBotConfig => {
        if (instagramBotConfig !== undefined) {
          this.botConfigService.createInstagramBot(instagramBotConfig).subscribe(apiResponse => {
            if (apiResponse.code === 200) {
              this.showSuccessToast('Success', 'Bot with key ' + botKey + ' created');
              this.updateBotConfigView();
            } else {
              this.showErrorToast('Error', apiResponse.message);
            }
          });
        }
      });
    } else if (botKey === BotKey.GOOGLE_SENTIMENT) {
      this.setupDialogService.openGoogleSentimentSetupDialog().afterClosed().subscribe(googleSentimentBotConfig => {
        if (googleSentimentBotConfig !== undefined) {
          this.botConfigService.createGoogleSentimentBot(googleSentimentBotConfig).subscribe(apiResponse => {
            if (apiResponse.code === 200) {
              this.showSuccessToast('Success', 'Bot with key ' + botKey + ' created');
              this.updateBotConfigView();
            } else {
              this.showErrorToast('Error', apiResponse.message);
            }
          });
        }
      })
    }
  }

  openBotDetailsDialog(botConfigId: string): void {
    this.botConfigService.getBotDetails(botConfigId).subscribe(apiResponse => {
      this.setupDialogService.openBotDetailsDialog(apiResponse.data);
    });
  }

  activate(botConfigId: string): void {
    this.botConfigService.activateBotConfig(botConfigId).subscribe(apiResponse => {
      if (apiResponse.code === 200) {
        this.showSuccessToast('Success', 'Bot configuration  ' + botConfigId + ' activated!');
        this.updateBotConfigView();
      } else {
        this.showErrorToast('Error', apiResponse.message);
      }
    });
  }

  deactivate(botConfigId: string): void {
    this.botConfigService.deactivateBotConfig(botConfigId).subscribe(apiResponse => {
      if (apiResponse.code === 200) {
        this.showSuccessToast('Success', 'Bot configuration ' + botConfigId + ' deactivated!');
        this.updateBotConfigView();
      } else {
        this.showErrorToast('Error', apiResponse.message);
      }
    });
  }

  updateBotConfigView(): void {
    this.botConfigService.getAllBotDetails().subscribe(apiResponse => {
      if (apiResponse.code === 200) {
        this.botDetails = apiResponse.data as BotDetails[];
      }
    });
  }

  showSuccessToast(title: string, message: string): void {
    this.toastService.success(message, title, {positionClass: 'toast-bottom-left'});
  }

  showErrorToast(title: string, message: string): void {
    this.toastService.error(message, title, {positionClass: 'toast-bottom-left'});
  }
}
