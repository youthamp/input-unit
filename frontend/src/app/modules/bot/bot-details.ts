export interface BotDetails {
  id: string;
  name: string;
  botKey: string;
  cron: string;
  lastExecution: string;
  errorMsg: string;
  manualExecution: boolean;
  active: boolean;
  lastExecutionError: boolean;
  executionCount: number;
  properties: Map<string, string>;
}

export class BotKey {
  public static readonly TWITTER: string = 'twitter';
  public static readonly INSTAGRAM: string = 'instagram';
  public static readonly GOOGLE_SENTIMENT: string = 'google_sentiment';
}

export class BotGroup {
  public static readonly SOCIAL_NETWORK: string = 'social_network';
  public static readonly GOOGLE_CLOUD: string = 'google_cloud';
}

export class ExecutionType {
  public static readonly MANUAL: string = 'manual';
  public static readonly SCHEDULE: string = 'schedule';
}

export class SearchType {
  public static readonly PERSONAL_TIMELINE: string = 'personal timeline';
  public static readonly USER_TIMELINE: string = 'user timeline';
  public static readonly HASHTAGS: string = 'hashtags';
}

export interface ApiResponse {
  data: object;
  message: string;
  status: object;
  code: number;
}
