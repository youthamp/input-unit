import {Inject, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {ApiResponse, BotGroup, BotKey, ExecutionType} from './bot-details';
import {environment} from '../../../environments/environment';
import {TwitterDefaultProperties, TwitterSetupDialogConfig} from './dialogs/twitter-bot-setup/twitter-bot-types';
import {InstagramSetupDialogConfig} from './dialogs/instagram-bot-setup/instagram-bot-types';
import {GoogleSentimentSetupDialogConfig} from './dialogs/google-sentiment-setup/google-sentiment-types';

@Injectable({
  providedIn: 'root'
})
export class BotService {

  private readonly url: string = environment.backendUrl + '/youthamp/input-unit/bot';

  constructor(@Inject(HttpClient) private httpClient: HttpClient) {

  }

  getAllBotDetails(): Observable<ApiResponse> {
    return this.httpClient.get<ApiResponse>(this.url + '/details');
  }

  getBotDetails(botConfigId: string): Observable<ApiResponse> {
    return this.httpClient.get<ApiResponse>(this.url + '/details/' + botConfigId);
  }

  deleteBotConfig(id: string): Observable<any> {
    const httpParams = new HttpParams().set('botConfigId', id);
    const options = {params: httpParams};
    return this.httpClient.delete(this.url + '/configuration', options);
  }

  archiveBotConfig(id: string): Observable<any> {
    const httpParams = new HttpParams().set('botConfigId', id);
    const options = {params: httpParams};
    return this.httpClient.get(this.url + '/configuration/archive', options);
  }

  createTwitterBot(twitterBotConfig: TwitterSetupDialogConfig): Observable<ApiResponse> {

    const botConfigRequestBody = {
      botKey: BotKey.TWITTER,
      botGroup: BotGroup.SOCIAL_NETWORK,
      cron: twitterBotConfig.cron,
      manualExecution: twitterBotConfig.executionType === ExecutionType.MANUAL,
      properties: this.createTwitterBotConfigProperties(twitterBotConfig)
    };
    return this.httpClient.post<ApiResponse>(this.url + '/configuration', botConfigRequestBody);
  }

  createInstagramBot(instagramBotConfig: InstagramSetupDialogConfig): Observable<ApiResponse> {
    const botConfigRequestBody = {
      botKey: BotKey.INSTAGRAM,
      botGroup: BotGroup.SOCIAL_NETWORK,
      cron: instagramBotConfig.cron,
      manualExecution: instagramBotConfig.executionType === ExecutionType.MANUAL,
      properties: this.createInstagramBotConfigProperties(instagramBotConfig)
    };
    return this.httpClient.post<ApiResponse>(this.url +  '/configuration', botConfigRequestBody);
  }

  createGoogleSentimentBot(googleSentimentBotConfig: GoogleSentimentSetupDialogConfig): Observable<ApiResponse> {
    const botConfigRequestBody = {
      botKey: BotKey.GOOGLE_SENTIMENT,
      botGroup: BotGroup.GOOGLE_CLOUD,
      manualExecution: googleSentimentBotConfig.executionType === ExecutionType.MANUAL,
      properties: this.createGoogleSentimentBotProperties(googleSentimentBotConfig)
    };
    return this.httpClient.post<ApiResponse>(this.url +  '/configuration', botConfigRequestBody);
  }

  activateBotConfig(botConfigId: string): Observable<any> {
    return this.httpClient.get(this.url +  '/configuration/activate', this.createOptionsWithBotConfigId(botConfigId));
  }

  deactivateBotConfig(botConfigId: string): Observable<any> {
    return this.httpClient.get(this.url + '/configuration/deactivate', this.createOptionsWithBotConfigId(botConfigId));
  }

  createOptionsWithBotConfigId(botConfigId: string): any {
    return {params: new HttpParams().set('botConfigId', botConfigId)};
  }

  // assumed format of dateString 'HH:mm'
  private convertDurationStringToSeconds(durationString: string): number | null {
    try {
      const timeChunks = durationString.split(':');
      return Number(timeChunks[0]) * 60 * 60 + Number(timeChunks[1]) * 60;
    } catch (e) {
      return null;
    }
  }

  private createTwitterBotConfigProperties(twitterBotConfig: TwitterSetupDialogConfig): any {
    const properties = {searchType: '', hashtags: '', screenName: '', maxCount: TwitterDefaultProperties.DEFAULT_MAX_COUNT};

    if (twitterBotConfig.hashtags) {
      properties.hashtags = twitterBotConfig.hashtags;
    }

    if (twitterBotConfig.screenName) {
      properties.screenName = twitterBotConfig.screenName;
    }

    if (twitterBotConfig.searchType) {
      properties.searchType = twitterBotConfig.searchType;
    }

    if (twitterBotConfig.maxCount) {
      properties.maxCount = twitterBotConfig.maxCount;
    }

    return properties;
  }

  private createInstagramBotConfigProperties(instagramBotConfig: InstagramSetupDialogConfig): any {
    return {};
  }

  private createGoogleSentimentBotProperties(googleSentimentBotConfig: GoogleSentimentSetupDialogConfig): any {
      return {dataTypes: googleSentimentBotConfig.dataTypes};
  }
}
